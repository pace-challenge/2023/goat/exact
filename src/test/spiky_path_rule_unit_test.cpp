#include "../solver/rules.hpp"
#include "tester.hpp"
#include "../io/print_collection.hpp"

#include <vector>
#include <utility>
#include <iostream>
#include "../io/trigraph_outputer.hpp"
#include "../utils/utility.hpp"

using vertex_t = HashTrigraph::vertex_t;

using namespace std;

void check_g_contraction(Tester & t, const string & name, const HashTrigraph &g){

	t.check(name);
	{
        HashTrigraph gcopy = g;
        vector<pair<vertex_t,vertex_t>> contraction_sequence;
		spiky_path_rule(gcopy,contraction_sequence);
        t.is_true(gcopy.get_vertices_count() == 7);
		t.is_true(gcopy.get_edge(0,1) == HashTrigraph::edge_t::black_edge);
		t.is_true(gcopy.get_edge(0,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(gcopy.get_edge(0,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(gcopy.get_edge(1,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(gcopy.get_edge(1,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(gcopy.get_edge(2,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(contraction_sequence.size() == g.get_vertices_count() - gcopy.get_vertices_count());
        t.is_true(get_highest_red_degree_of_merges_partial(g,contraction_sequence) <= 2);
		t.ok();
	}
}

int main()
{
	Tester t;

    HashTrigraph g(10);
    //k4
    g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
    g.set_edge(0,2,HashTrigraph::edge_t::black_edge);
    g.set_edge(0,3,HashTrigraph::edge_t::black_edge);
    g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
    g.set_edge(1,3,HashTrigraph::edge_t::black_edge);
    g.set_edge(2,3,HashTrigraph::edge_t::black_edge);
    //path
    g.set_edge(0,4,HashTrigraph::edge_t::black_edge);
    g.set_edge(4,5,HashTrigraph::edge_t::black_edge);
    g.set_edge(5,6,HashTrigraph::edge_t::black_edge);
    g.set_edge(6,7,HashTrigraph::edge_t::black_edge);
    g.set_edge(7,8,HashTrigraph::edge_t::black_edge);
    g.set_edge(8,9,HashTrigraph::edge_t::black_edge);
    g.set_edge(9,2,HashTrigraph::edge_t::black_edge);

    check_g_contraction(t, "K4+P6", g);

    for(int i = 5; i <= 8; ++i){
        g.add_vertex(i+5);
        g.set_edge(i, i+5, HashTrigraph::edge_t::black_edge);
        g.add_vertex(i+9);
        g.set_edge(i+5, i+9, HashTrigraph::edge_t::red_edge);
    }

    check_g_contraction(t, "K4+P6+inner spikes", g);

    g.add_vertex(19);
    g.set_edge(4, 19, HashTrigraph::edge_t::black_edge);
    g.add_vertex(20);
    g.set_edge(19, 20, HashTrigraph::edge_t::red_edge);
    g.add_vertex(21);
    g.set_edge(9, 21, HashTrigraph::edge_t::black_edge);
    check_g_contraction(t, "K4+P6+all spikes", g);

    g.add_vertex(22);
    g.set_edge(0, 22, HashTrigraph::edge_t::black_edge);
    g.add_vertex(23);
    g.set_edge(22, 23, HashTrigraph::edge_t::black_edge);
    g.add_vertex(24);
    g.set_edge(23, 24, HashTrigraph::edge_t::black_edge);
    g.add_vertex(25);
    g.set_edge(24, 25, HashTrigraph::edge_t::black_edge);
    g.add_vertex(26);
    g.set_edge(25, 26, HashTrigraph::edge_t::black_edge);
    g.set_edge(26, 2, HashTrigraph::edge_t::black_edge);
    check_g_contraction(t, "K4+P6+all spikes+P5", g);

    g.add_vertex(27);
    g.set_edge(0, 27, HashTrigraph::edge_t::black_edge);
    g.add_vertex(28);
    g.add_vertex(29);
    g.set_edge(2, 28, HashTrigraph::edge_t::black_edge);
    g.set_edge(28, 29, HashTrigraph::edge_t::black_edge);
    check_g_contraction(t, "K4+P6+all spikes+P5+spikes near path", g);
    

    {
        t.check("short path");
        HashTrigraph short_path(5);
        short_path.set_edge(0,1,HashTrigraph::edge_t::black_edge);
        short_path.set_edge(0,2,HashTrigraph::edge_t::black_edge);
        short_path.set_edge(1,2,HashTrigraph::edge_t::black_edge);
        short_path.set_edge(2,3,HashTrigraph::edge_t::black_edge);
        short_path.set_edge(3,0,HashTrigraph::edge_t::black_edge);
        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        spiky_path_rule(short_path,contraction_sequence);
        t.is_true(short_path.get_vertices_count() == 5);
        t.is_true(short_path.get_edge(0,1) == HashTrigraph::edge_t::black_edge);
        t.is_true(short_path.get_edge(0,2) == HashTrigraph::edge_t::black_edge);
        t.is_true(short_path.get_edge(1,2) == HashTrigraph::edge_t::black_edge);
        t.is_true(short_path.get_edge(2,3) == HashTrigraph::edge_t::black_edge);
        t.is_true(short_path.get_edge(3,0) == HashTrigraph::edge_t::black_edge);
		t.is_true(contraction_sequence.size() == 0);
        t.ok();
    }

    {
        t.check("cycle");
        HashTrigraph cycle(6);
        HashTrigraph ccopy = cycle;
        cycle.set_edge(0, 1, HashTrigraph::edge_t::black_edge);
        cycle.set_edge(1, 2, HashTrigraph::edge_t::black_edge);
        cycle.set_edge(2, 3, HashTrigraph::edge_t::black_edge);
        cycle.set_edge(3, 4, HashTrigraph::edge_t::black_edge);
        cycle.set_edge(4, 5, HashTrigraph::edge_t::black_edge);
        cycle.set_edge(5, 0, HashTrigraph::edge_t::black_edge);
        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        spiky_path_rule(cycle,contraction_sequence);
        t.is_true(cycle.get_vertices_count() == 1);
		t.is_true(contraction_sequence.size() == 5);
        t.is_true(get_highest_red_degree_of_merges_partial(ccopy,contraction_sequence) <= 2);
        t.ok();
    }


    {
        t.check("articulation cycle");
        HashTrigraph articulation(12);
        HashTrigraph acopy = articulation;
        //k4
        articulation.set_edge(0,1,HashTrigraph::edge_t::black_edge);
        articulation.set_edge(0,2,HashTrigraph::edge_t::black_edge);
        articulation.set_edge(0,3,HashTrigraph::edge_t::black_edge);
        articulation.set_edge(1,2,HashTrigraph::edge_t::black_edge);
        articulation.set_edge(1,3,HashTrigraph::edge_t::black_edge);
        articulation.set_edge(2,3,HashTrigraph::edge_t::black_edge);
        //C7
        articulation.set_edge(3, 4, HashTrigraph::edge_t::black_edge);
        articulation.set_edge(4, 5, HashTrigraph::edge_t::black_edge);
        articulation.set_edge(5, 6, HashTrigraph::edge_t::black_edge);
        articulation.set_edge(6, 7, HashTrigraph::edge_t::black_edge);
        articulation.set_edge(7, 8, HashTrigraph::edge_t::black_edge);
        articulation.set_edge(8, 9, HashTrigraph::edge_t::black_edge);
        articulation.set_edge(9, 3, HashTrigraph::edge_t::black_edge);
        //spike
        articulation.set_edge(3, 10, HashTrigraph::edge_t::black_edge);
        articulation.set_edge(10, 11, HashTrigraph::edge_t::black_edge);

        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        spiky_path_rule(articulation,contraction_sequence);
        t.is_true(articulation.get_vertices_count() == 6);
        
		t.is_true(articulation.get_edge(0,1) == HashTrigraph::edge_t::black_edge);
		t.is_true(articulation.get_edge(0,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(articulation.get_edge(0,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(articulation.get_edge(1,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(articulation.get_edge(1,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(articulation.get_edge(2,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(contraction_sequence.size() == 6);
        t.is_true(get_highest_red_degree_of_merges_partial(acopy,contraction_sequence) <= 2);
        t.ok();
    }


}

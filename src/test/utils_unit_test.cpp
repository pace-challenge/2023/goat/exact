#include "tester.hpp"
#include "../utils/utility.hpp"
#include <vector>
#include <utility>


using namespace std;

int main()
{
	Tester t;
	t.check("get_highest_red_degree_of_merges on path");
	{
        HashTrigraph g(6);
        g.set_edge(0,1, HashTrigraph::edge_t::black_edge);
        g.set_edge(0,5, HashTrigraph::edge_t::black_edge);
        g.set_edge(1,2, HashTrigraph::edge_t::black_edge);
        g.set_edge(3,4, HashTrigraph::edge_t::black_edge);
        g.set_edge(2,3, HashTrigraph::edge_t::black_edge);
        vector<pair<vertex_t,vertex_t>> contraction_sequence = {
            {3,4},
            {0,2},
            {5,3},
            {0,5},
            {1,0}
            };  
        t.is_true(get_highest_red_degree_of_merges(g, contraction_sequence) == 2);
    }
    t.ok();
}

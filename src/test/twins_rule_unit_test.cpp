#include "../solver/rules.hpp"
#include "tester.hpp"
#include "../io/print_collection.hpp"

#include <vector>
#include <utility>
#include <iostream>
#include "../io/trigraph_outputer.hpp"
#include "../utils/utility.hpp"

using vertex_t = HashTrigraph::vertex_t;

using namespace std;

int main()
{
    Tester t;

    {
        t.check("cherry");
        HashTrigraph g(3);
        g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
        g.set_edge(0,2,HashTrigraph::edge_t::black_edge);

        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        twins_rule(g,contraction_sequence);

        t.is_true(g.get_vertices_count() == 1);
        t.is_true(contraction_sequence.size() == 2);
        t.ok();
    }

    {
        t.check("edge");
        HashTrigraph g(2);
        g.set_edge(0,1,HashTrigraph::edge_t::black_edge);

        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        twins_rule(g,contraction_sequence);

        t.is_true(g.get_vertices_count() == 1);
        t.is_true(contraction_sequence.size() == 1);
        t.ok();
    }

    {
        t.check("P4");
        HashTrigraph g(4);
        g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
        g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
        g.set_edge(2,3,HashTrigraph::edge_t::black_edge);

        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        twins_rule(g,contraction_sequence);

        t.is_true(g.get_vertices_count() == 4);
        t.is_true(contraction_sequence.size() == 0);
        t.ok();
    }

    {
        t.check("K4");
        HashTrigraph g(4);
        g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
        g.set_edge(0,2,HashTrigraph::edge_t::black_edge);
        g.set_edge(0,3,HashTrigraph::edge_t::black_edge);
        g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
        g.set_edge(1,3,HashTrigraph::edge_t::black_edge);
        g.set_edge(2,3,HashTrigraph::edge_t::black_edge);

        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        twins_rule(g,contraction_sequence);

        t.is_true(g.get_vertices_count() == 1);
        t.is_true(contraction_sequence.size() == 3);
        t.ok();
    }

    {
        t.check("fat P4");
        HashTrigraph g(8);
        g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
        g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
        g.set_edge(2,3,HashTrigraph::edge_t::black_edge);
        g.set_edge(4,5,HashTrigraph::edge_t::black_edge);
        g.set_edge(5,6,HashTrigraph::edge_t::black_edge);
        g.set_edge(6,7,HashTrigraph::edge_t::black_edge);

        g.set_edge(0,5,HashTrigraph::edge_t::black_edge);
        g.set_edge(1,6,HashTrigraph::edge_t::black_edge);
        g.set_edge(2,7,HashTrigraph::edge_t::black_edge);
        g.set_edge(1,4,HashTrigraph::edge_t::black_edge);
        g.set_edge(2,5,HashTrigraph::edge_t::black_edge);
        g.set_edge(3,6,HashTrigraph::edge_t::black_edge);

        vector<pair<vertex_t,vertex_t>> contraction_sequence;
        twins_rule(g,contraction_sequence);

        t.is_true(g.get_vertices_count() == 4);
        t.is_true(contraction_sequence.size() == 4);
        t.ok();
    }




}

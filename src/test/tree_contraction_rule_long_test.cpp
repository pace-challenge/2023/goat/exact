#include "../solver/rules.hpp"
#include "tester.hpp"
#include "../io/print_collection.hpp"
#include "../generator/tree_generator.hpp"
#include "../graph/hash_trigraph.hpp"
#include "../io/trigraph_outputer.hpp"
#include "../utils/utility.hpp"

#include <vector>
#include <utility>
#include <iostream>


using namespace std;

int main()
{
	Tester t;
	t.check("Random trees");
	{
		for(int i = 1; i < 100; ++i){
			tree_generator<HashTrigraph> tg(i);
			HashTrigraph g = tg.generate(1, [](HashTrigraph & g, int a, int b){g.set_edge(a,b, HashTrigraph::edge_t::black_edge);});
			HashTrigraph gcopy = g;
			// TrigraphOutputer to = TrigraphOutputer(1);
			// to.write(cout, g, HashTrigraph::edge_t::black_edge);
			vector<pair<vertex_t,vertex_t>> contraction_sequence;
			tree_contraction_rule(g,contraction_sequence);
			t.is_true(g.get_vertices_count() == 1);
			t.is_true(get_highest_red_degree_of_merges(gcopy, contraction_sequence) <= 2);
		}
		t.ok();
	}
}

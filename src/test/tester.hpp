#pragma once

#include <cassert>
#include <iostream>
#include <string>

class Tester{
    public:
        Tester():checking(false){}

        void check(std::string s){
            if(checking){
                std::cout << "cannot check two things at once\n";
                exit(1);
            }
            std::cout << "Testing " << s << " ";
            checking = true;
        }

        void ok(){
            std::cout << ' ' << "\033[32m" << "OK" << "\033[0m" << std::endl;
            checking = false;
        }

#define is_true(q) check_true(#q,(q))
        void check_true(std::string s, bool val){
            if(!val){
                std::cout << ' ';
                std::cout << "\033[31m";
                std::cout << "test '" << s << "' failed!" << std::endl;
                std::cout << "\033[0m";
                exit(1);
            }else{
                std::cout << '.';
                std::cout.flush();
            }
        }

    private:
        bool checking;
};

#include "../solver/rules.hpp"
#include "tester.hpp"
#include "../io/print_collection.hpp"
#include "../io/trigraph_outputer.hpp"
#include <vector>
#include <utility>
#include <iostream>
#include "../io/trigraph_outputer.hpp"
#include "../utils/utility.hpp"

using vertex_t = HashTrigraph::vertex_t;

using namespace std;

int main()
{
	Tester t;
	t.check("C3+P4");
	{
		HashTrigraph g(6);
		g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
		g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
		g.set_edge(0,2,HashTrigraph::edge_t::black_edge);
		g.set_edge(2,3,HashTrigraph::edge_t::black_edge);
		g.set_edge(3,4,HashTrigraph::edge_t::black_edge);
		g.set_edge(4,5,HashTrigraph::edge_t::black_edge);
        vector<pair<vertex_t,vertex_t>> contraction_sequence;
		tree_contraction_rule(g,contraction_sequence);
        t.is_true(g.get_vertices_count() == 5);
        t.is_true(g.get_edge(0,1) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(1,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(0,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(2,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(3,4) == HashTrigraph::edge_t::red_edge);
		t.is_true(contraction_sequence.size() == 1);
        t.is_true(contraction_sequence[0] == make_pair(4,5));
		t.ok();
	}

    t.check("C3+binary tree");
	{
		HashTrigraph g(10);
		HashTrigraph gcopy = g;
		g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
		g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
		g.set_edge(0,2,HashTrigraph::edge_t::black_edge);
		g.set_edge(2,3,HashTrigraph::edge_t::black_edge);
		g.set_edge(3,4,HashTrigraph::edge_t::black_edge);
		g.set_edge(4,5,HashTrigraph::edge_t::black_edge);
		g.set_edge(4,6,HashTrigraph::edge_t::black_edge);
		g.set_edge(3,7,HashTrigraph::edge_t::black_edge);
		g.set_edge(7,8,HashTrigraph::edge_t::black_edge);
		g.set_edge(7,9,HashTrigraph::edge_t::black_edge);
        vector<pair<vertex_t,vertex_t>> contraction_sequence;
		tree_contraction_rule(g,contraction_sequence);
        t.is_true(g.get_vertices_count() == 5);
        t.is_true(g.get_edge(0,1) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(1,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(0,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(2,3) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(3,4) == HashTrigraph::edge_t::red_edge);
		t.is_true(contraction_sequence.size() == 5);
        t.is_true(get_highest_red_degree_of_merges_partial(gcopy,contraction_sequence) <= 2);
		t.ok();
	}

    t.check("C3+short paths");
	{
		HashTrigraph g(8);
		HashTrigraph gcopy = g;
		g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
		g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
		g.set_edge(0,2,HashTrigraph::edge_t::black_edge);
		g.set_edge(2,3,HashTrigraph::edge_t::black_edge);
		g.set_edge(3,4,HashTrigraph::edge_t::black_edge);
		g.set_edge(2,5,HashTrigraph::edge_t::black_edge);
		g.set_edge(5,6,HashTrigraph::edge_t::black_edge);
		g.set_edge(2,7,HashTrigraph::edge_t::black_edge);
        vector<pair<vertex_t,vertex_t>> contraction_sequence;
		tree_contraction_rule(g,contraction_sequence);
        t.is_true(g.get_vertices_count() == 5);
        t.is_true(g.get_edge(0,1) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(1,2) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(0,2) == HashTrigraph::edge_t::black_edge);
		vertex_t close = g.has_vertex(3) ? 3 : 5;
		vertex_t further = g.has_vertex(4) ? 4 : 6;
		t.is_true(g.get_edge(2,close) == HashTrigraph::edge_t::black_edge);
		t.is_true(g.get_edge(close,further) == HashTrigraph::edge_t::red_edge);
		t.is_true(contraction_sequence.size() == 3);
        t.is_true(get_highest_red_degree_of_merges_partial(gcopy,contraction_sequence) <= 2);
		t.ok();
	}
}

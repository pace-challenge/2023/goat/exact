#include "../graph/hash_trigraph.hpp"
#include "../io/edge_list_graph_reader.hpp"

#include "tester.hpp"

#include <functional>
#include <sstream>


using vertex_t = HashTrigraph::vertex_t;
using namespace std;

void add_edge_wrapper(HashTrigraph& g, vertex_t u, vertex_t v)
{
	g.set_edge(u, v, HashTrigraph::edge_t::black_edge);
}

int main(void)
{
	Tester t;
	t.check("graph reading without offset");
	{
		stringstream input("p tww 3 2\n0 1\n1 2\n");
		HashTrigraph G = EdgeListGraphReader<HashTrigraph>(0, add_edge_wrapper).read(input);
		t.is_true(G.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
		t.is_true(G.get_edge(0, 2) == HashTrigraph::edge_t::no_edge);
		t.is_true(G.get_edge(1, 2) == HashTrigraph::edge_t::black_edge);
	}
	t.ok();
	
	
	t.check("graph reading with offset");
	{
		stringstream input("p tww 4 3\n1 2\n1 3\n1 4\n");
		HashTrigraph G = EdgeListGraphReader<HashTrigraph>(1, add_edge_wrapper).read(input);
		t.is_true(G.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
		t.is_true(G.get_edge(0, 2) == HashTrigraph::edge_t::black_edge);
		t.is_true(G.get_edge(0, 3) == HashTrigraph::edge_t::black_edge);
		t.is_true(G.get_edge(1, 2) == HashTrigraph::edge_t::no_edge);
		t.is_true(G.get_edge(1, 3) == HashTrigraph::edge_t::no_edge);
		t.is_true(G.get_edge(2, 3) == HashTrigraph::edge_t::no_edge);
	}
	t.ok();
	return 0;
}

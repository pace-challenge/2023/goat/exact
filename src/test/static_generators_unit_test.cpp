#include "../graph/hash_trigraph.hpp"
#include "../generator/static_generators.hpp"

#include "tester.hpp"
#include <functional>

using vertex_t = HashTrigraph::vertex_t;
using namespace std;

int main(void)
{
	function<void(HashTrigraph&, vertex_t, vertex_t)> add_edge = [](HashTrigraph& G, vertex_t a, vertex_t b) { G.set_edge(a, b, HashTrigraph::edge_t::black_edge); };
	Tester t;
	t.check("Generating paths");
	
	HashTrigraph P1 = generate_path<HashTrigraph>(1, add_edge);
	
	HashTrigraph P2 = generate_path<HashTrigraph>(2, add_edge);
	t.is_true(P2.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
	
	HashTrigraph P3 = generate_path<HashTrigraph>(3, add_edge);
	t.is_true(P3.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
	t.is_true(P3.get_edge(1, 2) == HashTrigraph::edge_t::black_edge);
	t.is_true(P3.get_edge(0, 2) == HashTrigraph::edge_t::no_edge);
	
	t.ok();
	
	t.check("Generating cycles");
	
	HashTrigraph C3 = generate_cycle<HashTrigraph>(3, add_edge);
	t.is_true(C3.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
	t.is_true(C3.get_edge(1, 2) == HashTrigraph::edge_t::black_edge);
	t.is_true(C3.get_edge(0, 2) == HashTrigraph::edge_t::black_edge);
	
	HashTrigraph C5 = generate_cycle<HashTrigraph>(5, add_edge);
	t.is_true(C5.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
	t.is_true(C5.get_edge(1, 2) == HashTrigraph::edge_t::black_edge);
	t.is_true(C5.get_edge(2, 3) == HashTrigraph::edge_t::black_edge);
	t.is_true(C5.get_edge(3, 4) == HashTrigraph::edge_t::black_edge);
	t.is_true(C5.get_edge(4, 0) == HashTrigraph::edge_t::black_edge);
	t.is_true(C5.get_edge_count(HashTrigraph::edge_t::black_edge) == 5);
	t.ok();
	
	t.check("Generating k-partite-graphs");
	
	HashTrigraph K3 = generate_clique<HashTrigraph>(3, add_edge);
	t.is_true(K3.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
	t.is_true(K3.get_edge(1, 2) == HashTrigraph::edge_t::black_edge);
	t.is_true(K3.get_edge(0, 2) == HashTrigraph::edge_t::black_edge);
	
	
	HashTrigraph K2_3 = generate_complete_k_partite<HashTrigraph>(5, {2, 3}, add_edge);
	t.is_true(K2_3.get_edge(0, 2) == HashTrigraph::edge_t::black_edge);
	t.is_true(K2_3.get_edge(0, 3) == HashTrigraph::edge_t::black_edge);
	t.is_true(K2_3.get_edge(0, 4) == HashTrigraph::edge_t::black_edge);
	t.is_true(K2_3.get_edge(1, 2) == HashTrigraph::edge_t::black_edge);
	t.is_true(K2_3.get_edge(1, 3) == HashTrigraph::edge_t::black_edge);
	t.is_true(K2_3.get_edge(1, 4) == HashTrigraph::edge_t::black_edge);
	t.is_true(K2_3.get_edge_count(HashTrigraph::edge_t::black_edge) == 6);
	t.ok();
	
	return 0;
}

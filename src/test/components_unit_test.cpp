#include <unordered_set>
#include <vector>
#include <numeric>
#include "../utils/utility.hpp"
#include "tester.hpp"



int main()
{
	Tester t;
	t.check("connected case");
	{
		HashTrigraph g;
		g.add_vertex(0);
		g.add_vertex(1);
		g.add_vertex(2);
		g.add_vertex(3);
		g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
		g.set_edge(1,2,HashTrigraph::edge_t::black_edge);
		g.set_edge(2,3,HashTrigraph::edge_t::black_edge);
		auto comps = get_components(g);
		t.is_true(comps.size() == 1);
		t.ok();
	}
	t.check("disconnected case");
	{
		HashTrigraph g;
		g.add_vertex(0);
		g.add_vertex(1);
		g.add_vertex(2);
		g.add_vertex(3);
		g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
		g.set_edge(2,3,HashTrigraph::edge_t::black_edge);
		auto comps = get_components(g);
		t.is_true(comps.size() == 2);
		t.ok();
	}
	t.check("isolated vertices");
	{
		HashTrigraph g;
		g.add_vertex(0);
		g.add_vertex(1);
		g.add_vertex(2);
		g.add_vertex(3);
		auto comps = get_components(g);
		t.is_true(comps.size() == 4);
		t.ok();
	}
	t.check("two isolated one other comp");
	{
		HashTrigraph g;
		g.add_vertex(0);
		g.add_vertex(1);
		g.add_vertex(2);
		g.add_vertex(3);
		g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
		auto comps = get_components(g);
		t.is_true(comps.size() == 3);
		t.ok();
	}
	
}

#include "../graph/hash_trigraph.hpp"

#include "tester.hpp"
#include <functional>

using vertex_t = HashTrigraph::vertex_t;
using namespace std;

int main(void)
{
	Tester t;
	t.check("graph construction");
	HashTrigraph G(4);
	
	t.is_true(G.get_vertices_count() == 4);
	t.ok();
	
	t.check("edges adding");
	G.set_edge(0, 1, HashTrigraph::edge_t::black_edge);
	t.is_true(G.validate_invariants());
	G.set_edge(0, 2, HashTrigraph::edge_t::red_edge);
	t.is_true(G.validate_invariants());
	G.set_edge(1, 2, HashTrigraph::edge_t::red_edge);
	t.is_true(G.validate_invariants());
	G.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
	t.is_true(G.validate_invariants());
	t.is_true(G.get_edge(0, 1) == HashTrigraph::edge_t::black_edge);
	t.is_true(G.get_edge(0, 2) == HashTrigraph::edge_t::red_edge);
	t.is_true(G.get_edge(0, 3) == HashTrigraph::edge_t::no_edge);
	t.is_true(G.get_edge(1, 0) == HashTrigraph::edge_t::black_edge);
	t.is_true(G.get_edge(1, 2) == HashTrigraph::edge_t::red_edge);
	t.is_true(G.get_edge(1, 3) == HashTrigraph::edge_t::black_edge);
	t.is_true(G.get_edge(2, 0) == HashTrigraph::edge_t::red_edge);
	t.is_true(G.get_edge(2, 1) == HashTrigraph::edge_t::red_edge);
	t.is_true(G.get_edge(2, 3) == HashTrigraph::edge_t::no_edge);
	t.is_true(G.get_edge(3, 0) == HashTrigraph::edge_t::no_edge);
	t.is_true(G.get_edge(3, 1) == HashTrigraph::edge_t::black_edge);
	t.is_true(G.get_edge(3, 2) == HashTrigraph::edge_t::no_edge);
	
	function<void(vertex_t)> functions[2] = {
			[&G](vertex_t v) { G.set_edge(v, v, HashTrigraph::edge_t::black_edge); },
			[&G](vertex_t v) { G.get_edge(v, v); }};
	for(auto f: functions)
	{
		for(vertex_t v: G.get_vertices())
		{
			bool caught = false;
			try
			{
				f(v);
			}
			catch(const std::exception& e)
			{
				caught = true;
			}
			t.is_true(caught);
		}
	}
	t.ok();
	
	t.check("changing edges");
	G.set_edge(0, 1, HashTrigraph::edge_t::red_edge);
	t.is_true(G.validate_invariants());
	G.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
	t.is_true(G.validate_invariants());
	t.is_true(G.get_edge(1, 0) == HashTrigraph::edge_t::red_edge);
	t.is_true(G.get_edge(2, 0) == HashTrigraph::edge_t::black_edge);
	G.set_edge(2, 0, HashTrigraph::edge_t::no_edge);
	t.is_true(G.validate_invariants());
	G.set_edge(0, 1, HashTrigraph::edge_t::no_edge);
	t.is_true(G.validate_invariants());
	t.is_true(G.get_edge(1, 0) == HashTrigraph::edge_t::no_edge);
	t.is_true(G.get_edge(2, 0) == HashTrigraph::edge_t::no_edge);
	t.ok();
	
	{
		t.check("max_if_merge");
		HashTrigraph g(4);
		g.set_edge(0, 1, HashTrigraph::edge_t::red_edge);
		t.is_true(G.validate_invariants());
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		t.is_true(G.validate_invariants());
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		t.is_true(G.validate_invariants());
		g.set_edge(1, 2, HashTrigraph::edge_t::black_edge);
		t.is_true(G.validate_invariants());
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		t.is_true(G.validate_invariants());
		// 2, 3 noedge
		t.is_true(g.max_if_merge(0, 1) == 0);
		t.is_true(g.max_if_merge(0, 2) == 2);
		t.is_true(g.max_if_merge(0, 3) == 2);
		t.is_true(g.max_if_merge(1, 2) == 2);
		t.is_true(g.max_if_merge(1, 3) == 2);
		t.is_true(g.max_if_merge(2, 3) == 1);
	}
	{
		HashTrigraph g(9);
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(2, 5, HashTrigraph::edge_t::red_edge);
		g.set_edge(2, 6, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 7, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 8, HashTrigraph::edge_t::red_edge);
		t.is_true(g.max_if_merge(0, 1) == 3);
		t.is_true(g.max_if_merge(1, 0) == 3);
	}
	{
		HashTrigraph g(9);
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(2, 5, HashTrigraph::edge_t::red_edge);
		g.set_edge(2, 6, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 7, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 8, HashTrigraph::edge_t::red_edge);
		
		g.set_edge(0,1,HashTrigraph::edge_t::black_edge);
		t.is_true(g.max_if_merge(0, 1) == 3);
		t.is_true(g.max_if_merge(1, 0) == 3);
	}
	{
		HashTrigraph g(9);
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(2, 5, HashTrigraph::edge_t::red_edge);
		g.set_edge(2, 6, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 7, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 8, HashTrigraph::edge_t::red_edge);
		
		g.set_edge(0,1,HashTrigraph::edge_t::red_edge);
		t.is_true(g.max_if_merge(0, 1) == 3);
		t.is_true(g.max_if_merge(1, 0) == 3);
	}
	{
		HashTrigraph g(9);
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(2, 5, HashTrigraph::edge_t::red_edge);
		g.set_edge(2, 6, HashTrigraph::edge_t::red_edge);
		g.set_edge(3, 7, HashTrigraph::edge_t::red_edge);
		g.set_edge(3, 8, HashTrigraph::edge_t::red_edge);
		t.is_true(g.max_if_merge(0, 1) == 3);
		t.is_true(g.max_if_merge(1, 0) == 3);
	}
	{
		HashTrigraph g(9);
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(4, 5, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 6, HashTrigraph::edge_t::red_edge);
		g.set_edge(3, 7, HashTrigraph::edge_t::red_edge);
		g.set_edge(3, 8, HashTrigraph::edge_t::red_edge);
		t.is_true(g.max_if_merge(0, 1) == 2);
		t.is_true(g.max_if_merge(1, 0) == 2);
	}
	{
		HashTrigraph g(9);
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(4, 5, HashTrigraph::edge_t::red_edge);
		g.set_edge(4, 6, HashTrigraph::edge_t::red_edge);
		g.set_edge(3, 7, HashTrigraph::edge_t::red_edge);
		g.set_edge(3, 8, HashTrigraph::edge_t::red_edge);
		g.merge(0,1);
		t.is_true(G.validate_invariants());
		g.merge(0,2);
		t.is_true(G.validate_invariants());
		g.merge(0,3);
		t.is_true(G.validate_invariants());
		g.merge(0,4);
		t.is_true(G.validate_invariants());
		g.merge(0,5);
		t.is_true(G.validate_invariants());
		g.merge(0,6);
		t.is_true(G.validate_invariants());
		g.merge(0,7);
		t.is_true(G.validate_invariants());
		g.merge(0,8);
		t.is_true(G.validate_invariants());
		t.ok();
	}
	
	{
		t.check("Extensive merging test");
		HashTrigraph g(9);
		g.set_edge(0, 2, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(0, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 3, HashTrigraph::edge_t::black_edge);
		g.set_edge(1, 4, HashTrigraph::edge_t::black_edge);
		g.set_edge(4, 5, HashTrigraph::edge_t::black_edge);
		g.set_edge(4, 6, HashTrigraph::edge_t::black_edge);
		g.set_edge(3, 7, HashTrigraph::edge_t::black_edge);
		g.set_edge(3, 8, HashTrigraph::edge_t::black_edge);
		for(auto u : g.get_vertices())
			for(auto v : g.get_vertices())
			{
				if(u == v)
					continue;
				auto promised_red_deg = g.max_if_merge(u,v);
				HashTrigraph copy1(g);
				copy1.merge(u,v);
				t.is_true(copy1.validate_invariants());
				HashTrigraph copy2(g);
				copy2.merge(v,u);
				t.is_true(copy1.validate_invariants());
				//std::cerr << u << " " << v << std::endl;
				t.is_true(promised_red_deg == copy1.get_max_degree(HashTrigraph::edge_t::red_edge));
				t.is_true(promised_red_deg == copy2.get_max_degree(HashTrigraph::edge_t::red_edge));
			}
		t.ok();
	}

	{
		t.check("max if merge on P6");
        HashTrigraph g(6);
        g.set_edge(0,1, HashTrigraph::edge_t::black_edge);
        g.set_edge(0,5, HashTrigraph::edge_t::black_edge);
        g.set_edge(1,2, HashTrigraph::edge_t::black_edge);
        g.set_edge(3,4, HashTrigraph::edge_t::black_edge);
        g.set_edge(2,3, HashTrigraph::edge_t::black_edge);
		t.is_true(g.max_if_merge(3,4) <= 1);
		g.merge(3,4);
		t.is_true(g.max_if_merge(0,2) <= 2);
		g.merge(0,2);
		t.is_true(g.max_if_merge(5,3) <= 2); // one of the degrees is already two (so we allow it even if max red degree after merge is 1)
		g.merge(5,3);
		t.is_true(g.max_if_merge(0,5) <= 1);
		g.merge(0,5);
		t.is_true(g.max_if_merge(1,0) <= 1); // one of the red degrees is already one (so we allow it even if it should be 0)
		g.merge(1,0);
    	t.ok();
    }
	
	return 0;
}

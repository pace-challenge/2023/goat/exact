#include <unordered_set>
#include <vector>
#include <numeric>
#include "../utils/cograph.hpp"
#include "tester.hpp"

class simple_graph
{
 public:
	using vertex_t = std::size_t;
	simple_graph(std::size_t n) : adjlist(n) { }
	
	const auto& get_vertices() const
	{
		//would be much better with something like std::views::iota...
		static std::vector <vertex_t> vertices(adjlist.size());
		std::iota(vertices.begin(), vertices.end(), 0);
		return vertices;
	}
	const std::unordered_set <vertex_t>& neighbours(vertex_t u) const
	{
		return adjlist[u];
	}
	bool has_edge(vertex_t u, vertex_t v) const
	{
		return adjlist[u].count(v);
	}
	void add_edge(vertex_t u, vertex_t v)
	{
		adjlist[u].insert(v);
		adjlist[v].insert(u);
	}
 private:
	std::vector <std::unordered_set<vertex_t>> adjlist;
};


int main()
{
	Tester t;
	t.check("P4");
	{
		simple_graph G(4);
		G.add_edge(0, 1);
		G.add_edge(1, 2);
		G.add_edge(2, 3);
		t.is_true(!is_cograph(G));
		t.ok();
	}
	
	t.check("P4 with edge");
	{
		simple_graph G(4);
		G.add_edge(0, 1);
		G.add_edge(1, 2);
		G.add_edge(2, 3);
		G.add_edge(0, 3);
		t.is_true(is_cograph(G));
		t.ok();
	}
	t.check("P4 with other edge");
	{
		simple_graph G(4);
		G.add_edge(0, 1);
		G.add_edge(1, 2);
		G.add_edge(2, 3);
		G.add_edge(0, 2);
		t.is_true(is_cograph(G));
		t.ok();
	}
	t.check("Fixed cograph");
	{
		//a b c d e f g
		//0 1 2 3 4 5 6
		//this cograph: https://www.researchgate.net/figure/Example-of-a-cograph-and-its-cotree-representation_fig3_220397650
		simple_graph G(7);
		G.add_edge(0,2);
		G.add_edge(2,3);
		G.add_edge(3,1);
		G.add_edge(1,5);
		G.add_edge(5,4);
		G.add_edge(4,0);
		G.add_edge(0,6);
		G.add_edge(6,1);
		G.add_edge(1,2);
		G.add_edge(0,3);
		G.add_edge(0,5);
		G.add_edge(1,4);
		t.is_true(is_cograph(G));
		t.ok();
	}
	
}

#pragma once

#include "hash_trigraph.hpp"

#include "../fast_hash_table.hpp"
#include <cstddef>
#include <stdexcept>


class HashTrigraph
{
 public:
	HashTrigraph(size_t n = 0);
	using vertex_t = int;
	enum class edge_t : unsigned char
	{
		no_edge = 0,
		black_edge = 1,
		red_edge = 2,
		any = 3
	};
	bool has_vertex(vertex_t v) const;
	void remove_vertex(vertex_t v);
	void add_vertex(vertex_t label);
	
	const fast_set<vertex_t>& get_vertices() const;
	size_t get_vertices_count() const;
	/**
	 * @brief Get red or black neighbours of v
	 * @param type red/black
	 */
	const fast_set<vertex_t>& get_neighbours(vertex_t v, edge_t type) const;
	
	size_t get_degree(vertex_t v, edge_t type) const;
	size_t get_max_degree(edge_t type) const;
	
	
	/**
	 * if edge does not exist then creates it
	 * if it exists then changes the edge color (if was not the same as `type`)
	 */
	/**
	 * @brief Sets edge to @value type. If no edge between u,v existed, creates one.
	 */
	void set_edge(vertex_t u, vertex_t v, edge_t type);
	edge_t get_edge(vertex_t u, vertex_t v) const;
	size_t get_edge_count(edge_t type) const;
	
	/*
	 * Merges u into v -- u remains, v is deleted
	 */
	void merge(vertex_t u, vertex_t v);
	/*
	 * Highest red degree that will be created by the merge operation (either the degree of u/v or one of the newly formed red edge)
	 */
	size_t max_if_merge(vertex_t u, vertex_t v) const;
	
	/**
	 * Induces a subgraph on given range of vertices. If there is a vertex in the range which is not in the graph, aborts.
	 */
	template<typename InputIt>
	HashTrigraph induced_subgraph(InputIt begin, InputIt end) const
	{
		HashTrigraph g;
		for(; begin != end; ++begin)
		{
			if(!has_vertex(*begin))
				throw std::invalid_argument("Nonexistent vertex in induced subgraph");
			g.add_vertex(*begin);
		}
		for(vertex_t v: g.get_vertices())
			for(auto&& edge_type: {edge_t::black_edge, edge_t::red_edge})
				for(vertex_t s: get_neighbours(v, edge_type))
					if(g.vertices.count(s) == 1)
						g.set_edge(v, s, edge_type);
		return g;
	}
	HashTrigraph complement() const;
	
	bool validate_invariants();
	
	size_t get_complement_edge_count();
 private:
	fast_set<vertex_t> vertices;
	fast_map<vertex_t, fast_set<vertex_t>> black_edges;
	fast_map<vertex_t, fast_set<vertex_t>> red_edges;
};
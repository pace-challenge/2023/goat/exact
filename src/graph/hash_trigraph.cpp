#include "hash_trigraph.hpp"

#include <stdexcept>
#include <vector>
#include <numeric>

using std::vector;

using vertex_t = HashTrigraph::vertex_t;

HashTrigraph::HashTrigraph(size_t n) : vertices(n), black_edges(n), red_edges(n)
{
	for(size_t i = 0; i < n; ++i)
	{
		vertices.insert(i);
		black_edges[i];
		red_edges[i];
	}
}

void HashTrigraph::remove_vertex(vertex_t v)
{
	for(vertex_t u: black_edges[v])
		black_edges[u].erase(v);
	black_edges.erase(v);
	for(vertex_t u: red_edges[v])
		red_edges[u].erase(v);
	red_edges.erase(v);
	vertices.erase(v);
}

const fast_set<vertex_t>& HashTrigraph::get_vertices() const
{
	return vertices;
}

size_t HashTrigraph::get_vertices_count() const
{
	return vertices.size();
}

const fast_set<vertex_t>& HashTrigraph::get_neighbours(vertex_t v, edge_t type) const
{
	if(!(type == edge_t::red_edge || type == edge_t::black_edge))
	{
		throw std::invalid_argument("Provide either red or black edge arg");
	}
	if(type == edge_t::red_edge)
		return red_edges.at(v);
	else
		return black_edges.at(v);
}

size_t HashTrigraph::get_degree(vertex_t v, edge_t type) const
{
	switch(type)
	{
		case edge_t::red_edge:
			return red_edges.at(v).size();
		case edge_t::black_edge:
			return black_edges.at(v).size();
		case edge_t::any:
			return red_edges.at(v).size() + black_edges.at(v).size();
		default:
			return 0;
	}
}

size_t HashTrigraph::get_max_degree(edge_t type) const
{
	size_t maxDegree = 0;
	for(auto v: vertices)
		maxDegree = std::max(maxDegree, get_degree(v, type));
	return maxDegree;

	//todo: is there a speed difference?
	// return std::transform_reduce(
	//         vertices.begin(), vertices.end(),
	//         0, std::max<size_t>,
	//         [&](vertex_t v){return get_degree(v, type);});
}
/*
size_t HashTrigraph::get_max_degree(edge_t type, vector<vertex_t>& vertices) const
{
	size_t maxDegree = get_max_degree(type);
	for(auto v: vertices)
		if(get_degree(v, type) == maxDegree)
			vertices.push_back(v);
	return maxDegree;
}*/


//---------------------------------------------------------------------------

void HashTrigraph::set_edge(vertex_t u, vertex_t v, edge_t value)
{
	if(u == v)
	{
		throw std::invalid_argument("Self cycles are not allowed.");
	}
	black_edges[u].erase(v), black_edges[v].erase(u);
	red_edges[u].erase(v), red_edges[v].erase(u);
	switch(value)
	{
		case edge_t::black_edge:
			black_edges[u].insert(v);
			black_edges[v].insert(u);
			break;
		case edge_t::red_edge:
			red_edges[u].insert(v);
			red_edges[v].insert(u);
			break;
		default:
			break;
	}
}

HashTrigraph::edge_t HashTrigraph::get_edge(vertex_t u, vertex_t v) const
{
	if(u == v)
	{
		throw std::invalid_argument("Self cycles are not allowed.");
	}
	if(black_edges.at(u).count(v) == 1)
	{
		return edge_t::black_edge;
	}
	else if(red_edges.at(u).count(v) == 1)
	{
		return edge_t::red_edge;
	}
	return edge_t::no_edge;
}

size_t HashTrigraph::get_edge_count(edge_t type) const
{

	if(type == edge_t::black_edge || type == edge_t::red_edge)
	{
		const auto& edges = type == edge_t::black_edge ? black_edges : red_edges;
		size_t sumDegree = 0;
		for(const auto& [v, neigh]: edges)
			sumDegree += neigh.size();
		return sumDegree / 2;;
	}

	if(type == edge_t::any)
	{
		size_t sumDegree = 0;
		for(const auto& [v, neigh]: black_edges)
			sumDegree += neigh.size();
		for(const auto& [v, neigh]: red_edges)
			sumDegree += neigh.size();
		return sumDegree / 2;;
	}

	return 0;
}

//-------------------------------------------------------

void HashTrigraph::merge(vertex_t u, vertex_t v)
{
	if(u == v)
	{
		throw std::invalid_argument("Merging vertex into itself is not allowed.");
	}
	if(get_vertices().count(u) == 0 || get_vertices().count(v) == 0)
	{
		throw std::invalid_argument("Merging not existing vertices");
	}
	set_edge(u, v, edge_t::no_edge);
	fast_set<vertex_t> red_neighbors;
	fast_set<vertex_t> black_neighbors;
	for(vertex_t w: black_edges[u])
		if(black_edges[v].count(w) == 0)
			red_neighbors.insert(w);
		else
			black_neighbors.insert(w);
	for(vertex_t w: black_edges[v])
		if(black_edges[u].count(w) == 0)
			red_neighbors.insert(w);
		else
			black_neighbors.insert(w);
	for(vertex_t w: red_edges[u])
		red_neighbors.insert(w);
	for(vertex_t w: red_edges[v])
		red_neighbors.insert(w);
	remove_vertex(u);
	remove_vertex(v);
	add_vertex(u);
	for(vertex_t w: red_neighbors)
		set_edge(u, w, edge_t::red_edge);
	for(vertex_t w: black_neighbors)
		set_edge(u, w, edge_t::black_edge);
}


size_t HashTrigraph::max_if_merge(vertex_t u, vertex_t v) const
{
	if(u == v)
	{
		throw std::invalid_argument("Merging vertex into itself is not allowed.");
	}
	size_t external_degree = 0;
	for(auto w: get_neighbours(v, edge_t::black_edge))
	{
		if(w == u)
			continue;
		size_t red_degree = get_degree(w, edge_t::red_edge);
		//if {u,w} is black_edge, this is correct degree, no more edges will be added
		//if {u,w} is red_edge, this is correct degree -- the one existent edge was already calculated in get_degree()
		//if {u,w} is not edge, increment by 1
		if(get_edge(u, w) == edge_t::no_edge)
			++red_degree;
		external_degree = std::max(external_degree, red_degree);
	}
	for(auto w: get_neighbours(v, edge_t::red_edge))
	{
		if(w == u)
			continue;
		size_t red_degree = get_degree(w, edge_t::red_edge);
		//if {u,w} is not an edge, this is correct, the newly created red degree is correct
		//if {u,w} is black edge, this is correct, the red degree is correct
		//if {u,w} is red edge, the two red edges were calculated twice
		if(get_edge(u, w) == edge_t::red_edge)
			--red_degree;
		external_degree = std::max(external_degree, red_degree);
	}

	//symetriccal work with neighbours of u


	for(auto w: get_neighbours(u, edge_t::black_edge))
	{
		if(w == v)
			continue;
		size_t red_degree = get_degree(w, edge_t::red_edge);
		//if {v,w} is black_edge, this is correct degree, no more edges will be added
		//if {v,w} is red_edge, this is correct degree -- the one existent edge was already calculated in get_degree()
		//if {v,w} is not edge, increment by 1
		if(get_edge(v, w) == edge_t::no_edge)
			++red_degree;
		external_degree = std::max(external_degree, red_degree);
	}
	for(auto w: get_neighbours(u, edge_t::red_edge))
	{
		if(w == v)
			continue;
		size_t red_degree = get_degree(w, edge_t::red_edge);
		//if {v,w} is not an edge, this is correct, the newly created red degree is correct
		//if {v,w} is black edge, this is correct, the red degree is correct
		//if {v,w} is red edge, the two red edges were calculated twice
		if(get_edge(v, w) == edge_t::red_edge)
			--red_degree;
		external_degree = std::max(external_degree, red_degree);
	}

	size_t merge_degree = 0;
	//todo: implement this more effectively without additional set?
	fast_set<vertex_t> common_neighbours;
	for(auto&& edge_type: {edge_t::black_edge, edge_t::red_edge})
	{
		for(auto w: get_neighbours(u, edge_type))
			common_neighbours.insert(w);
		for(auto w: get_neighbours(v, edge_type))
			common_neighbours.insert(w);
	}
	common_neighbours.erase(v);
	common_neighbours.erase(u);

	for(auto w: common_neighbours)
		if(!(get_edge(u, w) == edge_t::black_edge && get_edge(v, w) == edge_t::black_edge))
			merge_degree++;
	return std::max(external_degree, merge_degree);
}
void HashTrigraph::add_vertex(vertex_t label)
{
	red_edges[label];
	black_edges[label];
	vertices.insert(label);
}
bool HashTrigraph::has_vertex(vertex_t v) const
{
	return vertices.count(v) == 1;
}
bool HashTrigraph::validate_invariants()
{
	//have adj list -> have vertex
	for(auto& [v, adj_list]: black_edges)
		if(vertices.count(v) == 0)
			return false;
	for(auto& [v, adj_list]: red_edges)
		if(vertices.count(v) == 0)
			return false;

	//have u-v any  ->  have v-u any
	for(auto& [u, adj_list]: black_edges)
		for(auto& v: adj_list)
			if(black_edges[v].count(u) == 0)
				return false;

	for(auto& [u, adj_list]: red_edges)
		for(auto& v: adj_list)
			if(red_edges[v].count(u) == 0)
				return false;

	//have red any -> no black any (and vice versa)
	for(auto& [u, adj_list]: black_edges)
		for(auto& v: adj_list)
			if(red_edges[u].count(v) == 1 || red_edges[v].count(v) == 1)
				return false;

	for(auto& [u, adj_list]: red_edges)
		for(auto& v: adj_list)
			if(black_edges[u].count(v) == 1 || black_edges[v].count(v) == 1)
				return false;


	return true;
}
HashTrigraph HashTrigraph::complement() const
{
	HashTrigraph result;
	for(auto& v: get_vertices())
		result.add_vertex(v);
	for(auto it = get_vertices().begin(); it != get_vertices().end(); ++it)
		for(auto jt = std::next(it); jt != get_vertices().end(); ++jt)
		{
			const auto& u = *it;
			const auto& v = *jt;
			switch(get_edge(u, v))
			{

				case edge_t::no_edge:
					result.set_edge(u, v, edge_t::black_edge);
					break;
				case edge_t::black_edge:
					break;
				case edge_t::red_edge:
					result.set_edge(u, v, edge_t::red_edge);
					break;
				case edge_t::any:
					throw std::invalid_argument("Invalid edge");
					break;
			}
		}
	return result;
}
size_t HashTrigraph::get_complement_edge_count()
{
	const auto n = get_vertices_count();
	const auto m = get_edge_count(edge_t::any);
	const auto r = get_edge_count(edge_t::red_edge);
	return n * (n - 1) / 2 - m + r;
}

#pragma once

#include <vector>
#include <queue>
#include "../graph/hash_trigraph.hpp"
#include <cassert>
#include "../timer/timer.h"

using vertex_t = HashTrigraph::vertex_t;

inline std::vector<HashTrigraph> get_components(const HashTrigraph& g)
{
	std::unordered_map<vertex_t, int> components;
	int comp = 0;
	for(vertex_t v: g.get_vertices())
	{
		if(components.count(v) == 0)
		{
			components[v] = comp;
			std::queue<vertex_t> q;
			q.push(v);
			while(!q.empty())
			{
				vertex_t u = q.front();
				q.pop();
				for(auto&& edge_type: {HashTrigraph::edge_t::black_edge, HashTrigraph::edge_t::red_edge})
					for(auto& w: g.get_neighbours(u, edge_type))
						if(components.count(w) == 0)
						{
							components[w] = comp;
							q.push(w);
						}
			}
			comp++;
		}
	}

	std::vector<std::vector<vertex_t>> component_vertices(comp);
	for(auto&& [v, component]: components)
		component_vertices[component].push_back(v);
	std::vector<HashTrigraph> result;
	for(auto&& component_vertex_set: component_vertices)
		result.push_back(g.induced_subgraph(component_vertex_set.begin(), component_vertex_set.end()));
	return result;
}



size_t get_highest_red_degree_of_merges_partial(const HashTrigraph& g, const std::vector<std::pair<vertex_t, vertex_t>> & merges) {
	size_t highest_red_degree = 0;
	HashTrigraph copy(g);
	for(auto merge : merges) {
		copy.merge(merge.first, merge.second);
		highest_red_degree = std::max(highest_red_degree, copy.get_degree(merge.first, HashTrigraph::edge_t::red_edge));
	}
	assert(copy.get_vertices_count() == g.get_vertices_count() - merges.size());
	return highest_red_degree;
}

size_t get_highest_red_degree_of_merges(const HashTrigraph& g, const std::vector<std::pair<vertex_t, vertex_t>> & merges) {
	// TREE_TIMER(_rt, "get_highest_red_degree_of_merges", "");

	size_t highest_red_degree = 0;

	HashTrigraph copy(g);
	// int _cnt = 0;
	for(auto merge : merges) {
		highest_red_degree = std::max(highest_red_degree, copy.max_if_merge(merge.first, merge.second));
		copy.merge(merge.first, merge.second);
		// _cnt++;
		// if(_cnt % 1000 == 0) {
		// 	std::cerr<<"get_highest_red_degree_of_merges: "<< _cnt << "/" << merges.size() << " : " << _rt.get_current_dur_ms()<<"ms"<<std::endl;
		// 	std::cerr<<"g.black_m="<<copy.get_edge_count(HashTrigraph::edge_t::black_edge) << ", g.red_m="<<copy.get_edge_count(HashTrigraph::edge_t::red_edge)<< std::endl;
		// }
	}
	assert(copy.get_vertices_count() == 1);
	return highest_red_degree;
}

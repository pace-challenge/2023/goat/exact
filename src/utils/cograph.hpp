#pragma once


template<typename SimpleGraph>
bool is_cograph(const SimpleGraph& G)
{
	//aiming to find induced path on x-v-u-y
	for(auto&& u: G.get_vertices())
		for(auto&& v: G.neighbours(u))
			for(auto&& x: G.neighbours(v))
			{
				if(x == u)
					continue;
				for(auto&& y: G.neighbours(u))
				{
					if(y == v || y == x)
						continue;
					if(!(G.has_edge(x, u) || G.has_edge(x, y) || G.has_edge(v, y)))
						return false;
				}
			}
	return true;
}

//TODO: function to reconstruct the contraction sequence if G is cograph


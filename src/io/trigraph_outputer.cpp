#include "trigraph_outputer.hpp"

TrigraphOutputer::TrigraphOutputer(int offset):offset(offset){}

void TrigraphOutputer::write(std::ostream & os, const HashTrigraph & graph, HashTrigraph::edge_t edge_type) const{
    os << "p tww " << graph.get_vertices_count() << " " << graph.get_edge_count(edge_type) << std::endl;
    for(HashTrigraph::vertex_t u : graph.get_vertices()){
        for(HashTrigraph::vertex_t v : graph.get_neighbours(u, edge_type)){
            if(u > v){
                continue;
            }
            os << u + offset << " " << v + offset<< std::endl;
        }
    }
}

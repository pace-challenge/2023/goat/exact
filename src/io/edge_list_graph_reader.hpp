#include <functional>
#include <istream>
#include <string>
#include <exception>

template<typename Graph>
class EdgeListGraphReader{
    public:
        // TODO FIX types to be derived from graph
        using graph_add_edge_fn = std::function<void(Graph&, int, int)>;
        EdgeListGraphReader(int offset, graph_add_edge_fn add_edge_fn):offset(offset), add_edge(add_edge_fn){}
        
        Graph read(std::istream & is) const{
            size_t n, m;
            std::string p, tww;
            is >> p >> tww >> n >> m;
            if (p != "p" || tww != "tww"){
                throw std::runtime_error("Invalid input format");
            }
            Graph G(n);
            for(size_t i = 0; i < m; ++i){
                int u, v;
                is >> u >> v;
                this->add_edge(G, u-offset, v-offset);
            }
            return G;
        }
    private:
        int offset;
        graph_add_edge_fn add_edge;
};
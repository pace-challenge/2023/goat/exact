#pragma once

#include <functional>
#include <ostream>

#include "../graph/hash_trigraph.hpp"

class TrigraphOutputer
{
 public:
	TrigraphOutputer(int offset);
	
	void write(std::ostream& os, const HashTrigraph& graph, HashTrigraph::edge_t edge_type) const;
 private:
	int offset;
};
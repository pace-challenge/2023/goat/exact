#include <iostream>
#include <unordered_map>
#include <vector>
#include <cassert>

#include "../generator/tree_generator.hpp"
#include "../generator/random_generator.hpp"


#include "../graph/hash_trigraph.hpp"
#include "../io/trigraph_outputer.hpp"

using namespace std;

unordered_map<string, size_t> num_args = {
    {"tree", 1},
    {"random", 2}
};

abstract_generator<HashTrigraph> * get_generator(const string & name, const vector<int> & args){
    if(num_args.at(name) != args.size()){
        throw invalid_argument("Invalid number of arguments of generator");
    }
    if(name == "tree"){
        return new tree_generator<HashTrigraph>(args[0]);
    }else if(name == "random"){
        return new random_generator<HashTrigraph>(args[0], args[1]);
    }
    throw "unknown generator";
}


int main(int argc, char ** argv){
    if(argc <= 2){
        cerr << "Run this generator like ./" << argv[0] << " generator_name seed arg_1 arg_2 ... arg_k" << endl;
        return 1;
    }
    int seed = atoi(argv[2]);
    vector<int> args;
    for(int i = 3; i < argc; ++i){
        args.push_back(atoi(argv[i]));
    }
    abstract_generator<HashTrigraph> * gen = get_generator(argv[1], args);
    HashTrigraph graph = gen->generate(seed, [](HashTrigraph & g, int a, int b){g.set_edge(a, b, HashTrigraph::edge_t::black_edge);});
    TrigraphOutputer TO(1);
    TO.write(cout, graph, HashTrigraph::edge_t::black_edge);
    return 0;
}
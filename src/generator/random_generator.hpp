#pragma once
#include <random>
#include <vector>
#include <utility>
#include <algorithm>
#include <stdexcept>

#include "abstract_generator.hpp"


template <typename Graph>
class random_generator : public abstract_generator<Graph>{
 public:
	/*!
	 * Constructor
	 * @param N - number of vertices
	 * @param M - number of edges
	 */
	random_generator(size_t _N, size_t _M) : N(_N), M(_M){
        if(M > N * (N - 1))
        {
            throw std::invalid_argument("too many edges, at most N*(N-1)");
        }
    }

	Graph generate(unsigned int seed, std::function<void(Graph &, int, int)> add_edge) override{	
        rng.seed(seed);
        std::vector<std::pair<int, int>> all_edges;
        //no self loops
        for(size_t i = 0; i < N; ++i){
            for(size_t j = i + 1; j < N; ++j){
                all_edges.emplace_back(i, j);
            }
        }
        std::shuffle(all_edges.begin(), all_edges.end(), rng);
        
        Graph graph(N);
        for(size_t i = 0; i < M; ++i){
            const auto&[u, v] = all_edges[i];
            add_edge(graph, u, v);
        }
        return graph;
    }
 private:
	size_t N;
	size_t M;
	std::mt19937 rng;
};





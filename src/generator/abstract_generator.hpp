#pragma once

#include <functional>

template <typename Graph>
class abstract_generator
{
 public:
	virtual ~abstract_generator() = default;
	virtual Graph generate(unsigned int seed, std::function<void(Graph &, int, int)> add_edge) = 0;
};
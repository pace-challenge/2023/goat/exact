#pragma once
#include <random>
#include <functional>
#include "abstract_generator.hpp"

template <typename Graph>
class tree_generator : public abstract_generator<Graph>{
public:
	/*!
	 * Constructor
	 * @param N - number of vertices
	 */
	tree_generator(size_t _N) : N(_N) {}
	Graph generate(unsigned int seed, std::function<void(Graph &, int, int)> add_edge) override{	
		rng.seed(seed);
		Graph g(N);
		for (size_t i = 1; i < N; ++i){
			std::uniform_int_distribution<> distribution(0, i-1);
			add_edge(g, i, distribution(rng));
		}
		return g;
	}

private:
	size_t N;
	size_t M;
	std::mt19937 rng;
};

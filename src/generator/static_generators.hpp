#include <functional>
#include <cassert>
#include <vector>
#include <numeric>

/*
 * Generates a path with @n vertices, edges are always from i to i+1
*/
template<typename Graph>
Graph generate_path(int n, std::function<void(Graph&, int, int)> add_edge){
    Graph result(n);
    for(int i = 0; i < n-1; ++i){
        add_edge(result, i, i+1);
    }
    return result;
}


/*
 * Generates a cycle with @n vertices, edges are always from i to i+1 mod n
*/
template<typename Graph>
Graph generate_cycle(int n, std::function<void(Graph&, int, int)> add_edge){
    Graph result = generate_path(n, add_edge);
    add_edge(result, n-1, 0);
    return result;
}


/*
 * Generates a complete graph
*/
template<typename Graph>
Graph generate_clique(int n, std::function<void(Graph&, int, int)> add_edge){
    return generate_complete_k_partite(n, std::vector<int>(n, 1), add_edge); 
}

/*
 * Generates a complete k-partite graph
*/
template<typename Graph>
Graph generate_complete_k_partite(int n, const std::vector<int> & groups, std::function<void(Graph&, int, int)> add_edge){
    Graph result(n);
    std::vector<int> group_add = {0};
    for(size_t i = 1; i < groups.size(); ++i){
        group_add.push_back(group_add[i-1] + groups[i-1]);
    }
    assert(group_add[groups.size()-1] + groups[groups.size()-1] == n);
    for(size_t group1 = 0; group1 < groups.size(); ++group1){
        for(size_t group2 = 0; group2 < group1; ++group2){
            for(int i = 0; i < groups[group1]; ++i){
                for(int j = 0; j < groups[group2]; ++j){
                    add_edge(result, i + group_add[group1], j + group_add[group2]);
                }
            }
        }   
    }
    return result;
}
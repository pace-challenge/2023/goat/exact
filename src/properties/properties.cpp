#include <iostream>
#include <string>
#include <unordered_map>
#include <functional>

#include "../io/edge_list_graph_reader.hpp"
#include "../graph/hash_trigraph.hpp"


using namespace std;

using vertex_t = HashTrigraph::vertex_t;

unordered_map<string, function<int(HashTrigraph)>> properties = {
    {"vertices",[](const HashTrigraph & g){return g.get_vertices_count();}},
    {"edges", [](const HashTrigraph & g){return g.get_edge_count(HashTrigraph::edge_t::black_edge);}},
    {"max_degree", [](const HashTrigraph & g){return g.get_max_degree(HashTrigraph::edge_t::black_edge);}}
};

// arg1-k: name of properties
int main(int argc, char ** argv){
    // listing all available
    if( argc == 2 && string(argv[1]) == "list"){
        std::vector<std::string> arguments;
        for(auto p : properties){
            arguments.push_back(p.first);
        }
        cout << arguments[0];
        for(size_t i = 1; i < arguments.size(); ++i){
            cout << "," << arguments[i];
        }
        cout << endl;
        return 0;
    }
    // if (argc == 1){
    //     cerr << "Run this program as ./" << argv[0] << "p1, p2, ..., pk, where pi is a name of graph property, the available properties are:" << endl;
    //     for(auto[p, _] : properties){
    //         cerr << p << " ";
    //     }
    //     cerr << endl;
    // }
    std::vector<std::string> arguments(argv + 1, argv + argc);
    if (arguments.size() == 0){
        for(auto p : properties){
            arguments.push_back(p.first);
        }
    }
    HashTrigraph G = EdgeListGraphReader<HashTrigraph>(0,
                        [](HashTrigraph & g, vertex_t u, vertex_t v){g.set_edge(u, v, HashTrigraph::edge_t::black_edge);}).read(std::cin);
    cout << properties.at(arguments[0])(G);
    for(size_t i = 1; i < arguments.size(); ++i){
        cout << "," << properties.at(arguments[i])(G);
    }
    cout << endl;
    return 0;
}
#include "bfs_tree_merging.hpp"
#include <map>
#include <set>
#include <algorithm>

vector<pair<vertex_t, vertex_t>> bfs_tree_merging(const HashTrigraph & g) {
    auto vs = g.get_vertices();

    size_t deg = 0;
    vertex_t deg_v;
    for(vertex_t v : g.get_vertices()) {
        size_t black_deg = g.get_degree(v, HashTrigraph::edge_t::black_edge);
        if(black_deg > deg) {
            deg = black_deg;
            deg_v = v;
        }
    }

    return bfs_tree_merging(g, deg_v);
}

void bfs_tree_merging_recursion(vertex_t v, const map<vertex_t, set<vertex_t>> & cs, vector<pair<vertex_t, vertex_t>> & merges_out) {
    if(cs.count(v) == 0) return;

    vertex_t w = -1;
    for(vertex_t u : cs.at(v)) {
        bfs_tree_merging_recursion(u, cs, merges_out);
        if(w != -1) {
            merges_out.push_back({u, w});
        }
        w = u;
    }
    merges_out.push_back({v, w});
}

vector<pair<vertex_t, vertex_t>> bfs_tree_merging(const HashTrigraph & g, vertex_t s) {
    queue<vertex_t> q;
    // parents
    map<vertex_t, vertex_t> p;
    // vertex to its children in the tree
    map<vertex_t, set<vertex_t>> cs;

    q.push(s);
    p[s] = -1;

    while(q.size()) {
        vertex_t v = q.front();
        q.pop();

        auto ns = g.get_neighbours(v, HashTrigraph::edge_t::black_edge);
        auto ns_red = g.get_neighbours(v, HashTrigraph::edge_t::red_edge);
        ns.insert(ns_red.begin(), ns_red.end());

        for(vertex_t u : ns) {
            if(p.count(u)) continue;
            p[u] = v;
            cs[v].insert(u);
            q.push(u);
        }
    }

    vector<pair<vertex_t, vertex_t>> merges;
    bfs_tree_merging_recursion(s, cs, merges);

    return merges;
}

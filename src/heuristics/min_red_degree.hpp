#pragma once

#include <iostream>
#include <vector>
#include <utility>
#include <queue>

#include "../graph/hash_trigraph.hpp"

using namespace std;

using vertex_t = HashTrigraph::vertex_t;

pair<pair<vertex_t, vertex_t>, size_t> get_min_red_degree_merge(HashTrigraph& g);

pair<std::vector<pair<vertex_t, vertex_t>>, size_t> solve_min_red_degree_heuristic(const HashTrigraph& g);


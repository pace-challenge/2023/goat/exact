#include "min_red_degree.hpp"

pair<pair<vertex_t, vertex_t>, size_t> get_min_red_degree_merge(HashTrigraph& g) {

    size_t min_red_redgree_if_merge = g.get_vertices_count();
    vertex_t mu = -1, mv = -1;

    for(auto uit = g.get_vertices().begin(); uit != g.get_vertices().end(); ++uit)
    {
        for(auto vit = std::next(uit); vit != g.get_vertices().end(); ++vit)
        {
            const auto& u = *uit;
            const auto& v = *vit;
            size_t merge_red_deg = g.max_if_merge(u, v);
            if(merge_red_deg < min_red_redgree_if_merge) {
                min_red_redgree_if_merge = merge_red_deg;
                mu = u;
                mv = v;
            }
        }
    }

    return {{mu, mv}, min_red_redgree_if_merge};
}

pair<std::vector<pair<vertex_t, vertex_t>>, size_t> solve_min_red_degree_heuristic(const HashTrigraph& g) {
    HashTrigraph g_copy(g);
    std::vector<pair<vertex_t, vertex_t>> merges;
    size_t max_red_degree = g.get_max_degree(HashTrigraph::edge_t::red_edge);

    // std::cerr<<"solve_min_red_degree_heuristic n="<<g_copy.get_vertices_count()<<", max_red_degree="<<max_red_degree<<endl;

    while(g_copy.get_vertices_count() > 1) {
        auto min_red_degree_merge_r = get_min_red_degree_merge(g_copy);
        // if(min_red_degree_merge_r.second > max_red_degree) {
        //     std::cerr<<"solve_min_red_degree_heuristic n="<<g_copy.get_vertices_count()<<", max_red_degree="<<min_red_degree_merge_r.second<<endl;
        // }
        max_red_degree = max(max_red_degree, min_red_degree_merge_r.second);
        merges.push_back(min_red_degree_merge_r.first);
        g_copy.merge(min_red_degree_merge_r.first.first, min_red_degree_merge_r.first.second);
    }

    return {merges, max_red_degree};
}

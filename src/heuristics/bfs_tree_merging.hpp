#pragma once

#include <iostream>
#include <vector>
#include <utility>
#include <queue>

#include "../graph/hash_trigraph.hpp"

using namespace std;

using vertex_t = HashTrigraph::vertex_t;

vector<pair<vertex_t, vertex_t>> bfs_tree_merging(const HashTrigraph & g);

vector<pair<vertex_t, vertex_t>> bfs_tree_merging(const HashTrigraph & g, vertex_t s);

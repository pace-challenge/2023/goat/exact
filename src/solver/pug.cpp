#include <iostream>
#include <vector>
#include <functional>
#include <string>

#include "../graph/hash_trigraph.hpp"
#include "../io/edge_list_graph_reader.hpp"
#include "../solver/rules.hpp"

using namespace std;

using vertex_t = HashTrigraph::vertex_t;
using reduction_rule = function<bool(HashTrigraph &, vector<pair<vertex_t, vertex_t>> &)>;


std::string graph_to_json(const HashTrigraph & g) {
    std::string s;
    s += "{\"vertices\":[";
    for(int v : g.get_vertices()) {
        s+=std::to_string(v)+",";
    }
    if(s[s.size() -1 ] != '['){
        s.pop_back();
    }
    s += "],\"edges\":[";
    for(int v : g.get_vertices()) {
        for(int u : g.get_neighbours(v, HashTrigraph::edge_t::black_edge)) {
            s+="["+std::to_string(v)+","+std::to_string(u)+",\"black\"],";
        }
        for(int u : g.get_neighbours(v, HashTrigraph::edge_t::red_edge)) {
            s+="["+std::to_string(v)+","+std::to_string(u)+", \"red\"],";
        }
    }
    if(s[s.size() -1 ] != '['){
        s.pop_back();
    }
    s += "]}";
    return s;
}

void output_graph_as_json(std::ostream& os,const HashTrigraph& graph){
	os << graph_to_json(graph);
}

int main()
{
    HashTrigraph graph = EdgeListGraphReader<HashTrigraph>(
                             1, [](HashTrigraph &g, vertex_t u, vertex_t v)
                             { g.set_edge(u, v, HashTrigraph::edge_t::black_edge); })
                             .read(std::cin);
    vector<pair<string, reduction_rule>> rules;
    rules.emplace_back("twin", twins_rule);
    rules.emplace_back("tree", tree_contraction_rule);
    rules.emplace_back("siblibg", sibling_rule);
    rules.emplace_back("spiky_path", spiky_path_rule);

    int n = graph.get_vertices_count();
    int m = graph.get_edge_count(HashTrigraph::edge_t::any);
    cerr << "start -> " << n << "," << m << endl;
    vector<pair<vertex_t, vertex_t>> merge_sequence;
    bool applied = true;
    while (applied)
    {
        applied = false;
        for (auto [name, rule] : rules)
        {
            rule(graph, merge_sequence);
            int nn = graph.get_vertices_count();
            int mm = graph.get_edge_count(HashTrigraph::edge_t::any);
            if (nn != n || mm != m)
            {
                cerr << name << " -> " << nn << ", " << mm << endl;
                n = nn;
                m = mm;
                applied = true;
                break;
            }
        }
    }

    output_graph_as_json(cout, graph);
}

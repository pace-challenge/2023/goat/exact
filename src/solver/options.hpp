#pragma once

struct Options {
    bool output_tww;
    bool no_output_solution;
    bool read_multiple_instances;
    std::string heuristic_type;
};

Options default_options() {
    Options options;
    options.output_tww = false;
    options.no_output_solution = false;
    options.read_multiple_instances = false;
    options.heuristic_type = "";
    return options;
}

Options read_options(int argc, char ** argv) {
    Options options = default_options();

    std::vector<std::string> tokens;
    for(int i = 1; i < argc; ++i) tokens.push_back(std::string(argv[i]));
    argc--;

    if(argc) {
        int i = 0;
        while(1) {
            if(i>=argc) break;

            if(tokens[i] == "--output_tww") {
                options.output_tww = true;
                i++;
                continue;
            }
            else if(tokens[i] == "--no_output_solution") {
                options.no_output_solution = true;
                i++;
                continue;
            }
            else if(tokens[i] == "--read_multiple_instances") {
                options.read_multiple_instances = true;
                i++;
                continue;
            }
            else if(tokens[i] == "--heuristic_type") {
                options.heuristic_type = std::string(tokens[i+1]);
                i++;
                i++;
                continue;
            }
            else {
                i++;
                continue;
                // throw std::runtime_error("Unknown option: " + tokens[i]);
            }
        }
    }

    return options;
}

#pragma once

#include "../graph/hash_trigraph.hpp"
#include <vector>

/*
 * Contracts hanging tree into black and red edge.
*/
bool tree_contraction_rule(HashTrigraph& g, std::vector<std::pair<HashTrigraph::vertex_t, HashTrigraph::vertex_t>>& merges);

bool sibling_rule(HashTrigraph& g, std::vector<std::pair<HashTrigraph::vertex_t, HashTrigraph::vertex_t>>& merges);

/*
 * Contracts spiky paths (path of length at least 3, where each vertex has at most 1 at most 2-path pending) into 3-paths.
 * The trees have to be contracted into spikes.
*/
bool spiky_path_rule(HashTrigraph &g, std::vector<std::pair<HashTrigraph::vertex_t, HashTrigraph::vertex_t>>& merges);

/*
 * Contracts true and false twins (only black edges).
 *
*/
bool twins_rule(HashTrigraph &g, std::vector<std::pair<HashTrigraph::vertex_t, HashTrigraph::vertex_t>>& merges);


#include "../rules.hpp"
#include <string>
#include <list>

using vertex_t = HashTrigraph::vertex_t;

//N(v) <= N(u)
bool is_subset1(const HashTrigraph& g, HashTrigraph::vertex_t u, HashTrigraph::vertex_t v)
{
    for(auto&& edge_type: {HashTrigraph::edge_t::red_edge, HashTrigraph::edge_t::black_edge})
        for(auto w: g.get_neighbours(v, edge_type))
            if(u != w && g.get_edge(u, w) == HashTrigraph::edge_t::no_edge)
                return false;
    return true;
}
//B(u) <= B(v)
bool is_subset2(const HashTrigraph& g, HashTrigraph::vertex_t u, HashTrigraph::vertex_t v)
{
    for(auto w: g.get_neighbours(u, HashTrigraph::edge_t::black_edge))
        if(v != w && g.get_edge(v, w) != HashTrigraph::edge_t::black_edge)
            return false;
    return true;
}
bool sibling_rule(HashTrigraph& g, std::vector<std::pair<HashTrigraph::vertex_t, HashTrigraph::vertex_t>>& merges)
{
    bool applied_atleast_once = false;
    bool applied;
    do
    {
        applied = false;
        std::list<vertex_t> vertices(g.get_vertices().begin(), g.get_vertices().end());
        for(auto it = vertices.begin(); it != vertices.end(); ++it)
        {
            for(auto jt = vertices.begin(); jt != vertices.end();)
            {
                const auto& u = *it;
                const auto& v = *jt;
                if(u != v && is_subset1(g, u, v) && is_subset2(g, u, v))
                {
                    g.merge(u, v);
                    merges.emplace_back(u, v);
                    applied = true;
                    applied_atleast_once = true;
                    jt = vertices.erase(jt);
                }
                else
                    ++jt;
            }
        }
    } while(applied);
    return applied_atleast_once;
}

const std::string & get_name();

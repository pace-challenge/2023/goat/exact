#include "../rules.hpp"

#include "../../graph/hash_trigraph.hpp"
#include "../../fast_hash_table.hpp"
#include <vector>
#include <queue>
#include <stdexcept>
#include <cassert>
#include <list>
#include <map>
#include <set>
#include <iostream>
#include "../../io/print_collection.hpp"

using namespace std;
using vertex_t = HashTrigraph::vertex_t;

using neighborhood_map_t = std::map<std::set<vertex_t>, std::set<vertex_t>>;

const bool DEBUG = false;

bool has_only_black_neighborhood(const HashTrigraph & g, vertex_t u) {
    return g.get_neighbours(u, HashTrigraph::edge_t::red_edge).size() == 0;
}

set<vertex_t> get_neighborhood(const HashTrigraph & g, vertex_t u) {
    auto Nbf = g.get_neighbours(u, HashTrigraph::edge_t::black_edge);
    return set<vertex_t>(Nbf.begin(), Nbf.end());
}

set<vertex_t> get_closed_neighborhood(const HashTrigraph & g, vertex_t u) {
    auto Nbf = get_neighborhood(g, u);
    Nbf.insert(u);
    return Nbf;
}

pair<neighborhood_map_t, neighborhood_map_t> construct_neighborhoods(const HashTrigraph& g) {
    neighborhood_map_t open_neighborhoods, closed_neighborhoods;

    for(int u : g.get_vertices()) {
        // No red edges allowed.
        if(!has_only_black_neighborhood(g, u)) {continue;}

        auto Nb = get_neighborhood(g, u);
        open_neighborhoods[Nb].insert(u);
        Nb.insert(u);
        closed_neighborhoods[Nb].insert(u);
    }

    return {open_neighborhoods, closed_neighborhoods};
}

void assert_neighborhods_are_valid(const HashTrigraph & g, const neighborhood_map_t & target_open_neighborhoods, const neighborhood_map_t & target_closed_neighborhoods) {
    auto [open_neighborhoods, closed_neighborhoods] = construct_neighborhoods(g);

    for(const auto & i : open_neighborhoods) {
        if(target_open_neighborhoods.count(i.first) == 0) {
            cerr<<"failed open_neighborhoods: ";
            cerr<<"-- not present"<<endl;
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- for_v: ";print(i.second, cerr);
            continue;
        }

        if(target_open_neighborhoods.at(i.first) != i.second) {
            cerr<<"failed open_neighborhoods: ";
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- should be: ";print(i.second, cerr);
            cerr<<"-- is: ";print(target_open_neighborhoods.at(i.first), cerr);
        }
    }
    for(const auto & i : target_open_neighborhoods) {
        if(open_neighborhoods.count(i.first) == 0) {
            cerr<<"failed target_open_neighborhoods: ";
            cerr<<"-- not present"<<endl;
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- for_v: ";print(i.second, cerr);
            continue;
        }

        if(open_neighborhoods.at(i.first) != i.second) {
            cerr<<"failed target_open_neighborhoods: ";
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- should be: ";print(i.second, cerr);
            cerr<<"-- is: ";print(open_neighborhoods.at(i.first), cerr);
        }
    }

    for(const auto & i : closed_neighborhoods) {
        if(target_closed_neighborhoods.count(i.first) == 0) {
            cerr<<"failed closed_neighborhoods: ";
            cerr<<"-- not present"<<endl;
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- for_v: ";print(i.second, cerr);
            continue;
        }

        if(target_closed_neighborhoods.at(i.first) != i.second) {
            cerr<<"failed closed_neighborhoods: ";
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- should be: ";print(i.second, cerr);
            cerr<<"-- is: ";print(target_closed_neighborhoods.at(i.first), cerr);
        }
    }
    for(const auto & i : target_closed_neighborhoods) {
        if(closed_neighborhoods.count(i.first) == 0) {
            cerr<<"failed target_closed_neighborhoods: ";
            cerr<<"-- not present"<<endl;
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- for_v: ";print(i.second, cerr);
            continue;
        }

        if(closed_neighborhoods.at(i.first) != i.second) {
            cerr<<"failed target_closed_neighborhoods: ";
            cerr<<"-- for: ";print(i.first, cerr);
            cerr<<"-- should be: ";print(i.second, cerr);
            cerr<<"-- is: ";print(closed_neighborhoods.at(i.first), cerr);
        }
    }


    assert(open_neighborhoods == target_open_neighborhoods);
    assert(closed_neighborhoods == target_closed_neighborhoods);
}

bool check_false_twins(vertex_t u, vertex_t v, const HashTrigraph & g) {
    auto Nu = get_neighborhood(g, u);
    auto Nv = get_neighborhood(g, v);
    return Nu == Nv;
}

bool check_true_twins(vertex_t u, vertex_t v, const HashTrigraph & g) {
    auto Nu = get_closed_neighborhood(g, u);
    auto Nv = get_closed_neighborhood(g, v);
    return Nu == Nv;
}

bool check_twins(vertex_t u, vertex_t v, const HashTrigraph & g) {
    return check_false_twins(u, v, g) || check_true_twins(u, v, g);
}

void clean_from_neighborhood_map(neighborhood_map_t & m, const std::set<vertex_t> & k, vertex_t v) {
    assert(m[k].count(v));
    m[k].erase(v);
    if(m[k].size() == 0) {
        m.erase(k);
    }
}


void contract_twins(
    vertex_t u,
    set<vertex_t> twins,
    HashTrigraph & g,
    neighborhood_map_t & open_neighborhoods,
    neighborhood_map_t & closed_neighborhoods,
    std::set<std::set<vertex_t>> & open_neighborhoods_q,
    std::set<std::set<vertex_t>> & closed_neighborhoods_q,
    vector<pair<vertex_t, vertex_t>>& merges_out
    ) {

    if(DEBUG) {
    std::cerr<<"contract_twins u="<< u<<std::endl;
    for(auto t : twins) {
        std::cerr<<" - " << t<< " : ";
        for(auto nt : get_neighborhood(g, t))std::cerr<<" "<<nt;
        std::cerr<<std::endl;
    }
    }

    if(DEBUG) {
        // Friendly check if we are doing the right things.
        for(vertex_t v : twins) {
            if(u == v) continue;
            assert(check_twins(u, v, g));
        }
    }

    set<vertex_t> true_N = get_neighborhood(g, u);
    for(vertex_t x : twins) {
        true_N.erase(x);
    }

    if(DEBUG){
    std::cerr<<"true_N=";
    for(auto t : true_N) {
        std::cerr<<" - " << t<< " : ";
        for(auto nt : get_neighborhood(g, t))std::cerr<<" "<<nt;
        std::cerr<<std::endl;
    }
    std::cerr<<endl;
    }

    // We need to modify the open_neighborhoods and closed_neighborhoods structure, so that they are still valid.
    // All twins have the same true neighborhod true_N.
    for(vertex_t nv : true_N) {
        // Only need to modify the vertices that have only black neighborhoods.
        if(g.get_neighbours(nv, HashTrigraph::edge_t::red_edge).size() != 0) continue;

        if(DEBUG){cerr<<"mod nv="<<nv<<endl;}
        // Modify open_neighborhoods.
        auto Nnv_open = get_neighborhood(g, nv);
        if(DEBUG){cerr<<"Nnv_open: ";print(Nnv_open, cerr);}
        assert(open_neighborhoods[Nnv_open].count(nv));
        // nv no longer has this neighborhoood.
        if(DEBUG){cerr<<"open_neighborhoods[Nnv_open]: ";print(open_neighborhoods[Nnv_open], cerr);}
        clean_from_neighborhood_map(open_neighborhoods, Nnv_open, nv);
        // We remove all twins except u.
        for(vertex_t v : twins) {if(v!=u)Nnv_open.erase(v);}
        if(DEBUG){cerr<<"Nnv_open2: ";print(Nnv_open, cerr);}

        // nv has now new neighborhood.
        if(DEBUG){cerr<<"open_neighborhoods[Nnv_open]3: ";print(open_neighborhoods[Nnv_open], cerr);}
        open_neighborhoods[Nnv_open].insert(nv);
        if(DEBUG){cerr<<"open_neighborhoods[Nnv_open]4: ";print(open_neighborhoods[Nnv_open], cerr);}
        if(open_neighborhoods[Nnv_open].size() > 1) open_neighborhoods_q.insert(Nnv_open);

        // Modify closed_neighborhoods.
        auto Nnv_closed = get_neighborhood(g, nv);
        Nnv_closed.insert(nv);
        if(DEBUG){cerr<<"Nnv_closed: ";print(Nnv_closed, cerr);}
        assert(closed_neighborhoods[Nnv_closed].count(nv));
        // nv no longer has this neighborhoood.
        if(DEBUG){cerr<<"closed_neighborhoods[Nnv_closed]: ";print(closed_neighborhoods[Nnv_closed], cerr);}
        clean_from_neighborhood_map(closed_neighborhoods, Nnv_closed, nv);
        // We remove all twins except u.
        for(vertex_t v : twins) {if(v!=u)Nnv_closed.erase(v);}
        if(DEBUG){cerr<<"Nnv_closed2: ";print(Nnv_closed, cerr);}
        // nv has now new neighborhood.
        if(DEBUG){cerr<<"closed_neighborhoods[Nnv_closed]3: ";print(closed_neighborhoods[Nnv_closed], cerr);}
        closed_neighborhoods[Nnv_closed].insert(nv);
        if(DEBUG){cerr<<"closed_neighborhoods[Nnv_closed]4: ";print(closed_neighborhoods[Nnv_closed], cerr);}
        if(closed_neighborhoods[Nnv_closed].size() > 1) closed_neighborhoods_q.insert(Nnv_closed);
    }


    if(DEBUG){cerr<<"twins: ";print(twins, cerr);}

    // Vertex u may from true twins or false twins.
    // In case of true twins, we need to further adjust the open/closed neighborhoods.
    auto Nu = get_neighborhood(g, u);
    clean_from_neighborhood_map(open_neighborhoods, Nu, u);
    for(vertex_t v : twins) {
        if(u == v) continue;
        Nu.erase(v);
    }
    open_neighborhoods[Nu].insert(u);
    if(open_neighborhoods[Nu].size() > 1) open_neighborhoods_q.insert(Nu);

    auto Ncu = get_neighborhood(g, u);
    Ncu.insert(u);
    clean_from_neighborhood_map(closed_neighborhoods, Ncu, u);
    for(vertex_t v : twins) {
        if(u == v) continue;
        Ncu.erase(v);
    }
    closed_neighborhoods[Ncu].insert(u);
    if(closed_neighborhoods[Ncu].size() > 1) closed_neighborhoods_q.insert(Ncu);

    // Now for each twin, we need to adjust the open/closed neighborhoods, by deleting them from them.
    for(vertex_t v : twins) {
        if(u == v) continue;
        if(DEBUG){std::cerr<<"remove: "<<v<<std::endl;}

        auto Nv = get_neighborhood(g, v);
        clean_from_neighborhood_map(open_neighborhoods, Nv, v);
        Nv.insert(v);
        clean_from_neighborhood_map(closed_neighborhoods, Nv, v);
    }

    for(vertex_t v : twins) {
        if(u == v) continue;
        if(DEBUG){std::cerr<<"remove2: "<<v<<std::endl;}
        // Merge v into u.
        merges_out.push_back({u, v});
        g.remove_vertex(v);
    }


    if(DEBUG){cerr<<"end check"<<endl;}

    if(DEBUG) {
        assert_neighborhods_are_valid(g, open_neighborhoods, closed_neighborhoods);
    }

    if(DEBUG){cerr<<"end check valid"<<endl;}

}

bool twins_rule(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges_out){

    // We first get all the eligible vertices with black neighborhood.

    auto [open_neighborhoods, closed_neighborhoods] = construct_neighborhoods(g);
    assert_neighborhods_are_valid(g, open_neighborhoods, closed_neighborhoods);

    std::set<std::set<vertex_t>> open_neighborhoods_q;
    for(auto & twins_i : open_neighborhoods) {
        const auto & twins = twins_i.second;
        if(twins.size() > 1) open_neighborhoods_q.insert(twins_i.first);
    }
    std::set<std::set<vertex_t>> closed_neighborhoods_q;
    for(auto & twins_i : closed_neighborhoods) {
        const auto & twins = twins_i.second;
        if(twins.size() > 1) closed_neighborhoods_q.insert(twins_i.first);
    }

    while(open_neighborhoods_q.size() || closed_neighborhoods_q.size()) {
        while(open_neighborhoods_q.size()) {
            auto twins_N = *open_neighborhoods_q.begin();
            open_neighborhoods_q.erase(open_neighborhoods_q.begin());

            if(open_neighborhoods.count(twins_N) == 0) continue;
            const auto & twins = open_neighborhoods.at(twins_N);
            if(twins.size() <= 1) continue;

            // We will contract everything into the first vertex u, i.e., remove everything from the graph except vertex u.
            vertex_t u = *twins.begin();
            contract_twins(u, twins, g, open_neighborhoods, closed_neighborhoods, open_neighborhoods_q, closed_neighborhoods_q, merges_out);
        }
        while(closed_neighborhoods_q.size()) {
            auto twins_N = *closed_neighborhoods_q.begin();
            closed_neighborhoods_q.erase(closed_neighborhoods_q.begin());

            if(closed_neighborhoods.count(twins_N) == 0) continue;
            const auto & twins = closed_neighborhoods.at(twins_N);
            if(twins.size() <= 1) continue;

            // We will contract everything into the first vertex u, i.e., remove everything from the graph except vertex u.
            vertex_t u = *twins.begin();
            contract_twins(u, twins, g, open_neighborhoods, closed_neighborhoods, open_neighborhoods_q, closed_neighborhoods_q, merges_out);
        }
    }

    // if(DEBUG) {
    // Final check if we missed anything.
    assert_neighborhods_are_valid(g, open_neighborhoods, closed_neighborhoods);
    for(auto & twins_i : open_neighborhoods) {assert(twins_i.second.size() <= 1);}
    for(auto & twins_i : closed_neighborhoods) {assert(twins_i.second.size() <= 1);}
    // }

    return false;
}

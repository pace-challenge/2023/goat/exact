#include "../rules.hpp"

#include "../../graph/hash_trigraph.hpp"
#include "../../fast_hash_table.hpp"
#include <vector>
#include <queue>
#include <stdexcept>
#include <cassert>
#include <list>

using namespace std;
using vertex_t = HashTrigraph::vertex_t;

/*
    * Returns k for which v is a kleaf, if v is not kleaf(or we do not know yet) returns -1.
*/
int calc_kleaf(const HashTrigraph & g, fast_map<vertex_t, int> &kleaves, vertex_t current){
    bool seen_non_kleaf = false;
    int largest_seen_k = -1;
    int prev_largest = -1;
    for(auto edge_type : {HashTrigraph::edge_t::black_edge, HashTrigraph::edge_t::red_edge}){
        for(auto v : g.get_neighbours(current, edge_type)){
            if (kleaves.count(v) == 0){
                if(seen_non_kleaf){
                    return -1;
                }
                seen_non_kleaf = true;
            } else{
                prev_largest = largest_seen_k;
                largest_seen_k = max(largest_seen_k, kleaves[v]);
            }
        }
    }
    return kleaves[current] = (seen_non_kleaf ? largest_seen_k + 1 : prev_largest +1 );
}

/*
    * Returns the only vertex that is not a kleaf for smaller k than the current vertex. 
    * If that vertex does not exists, returns -1.
    * Assumes current vertex is kleaf
*/
vertex_t get_parent(const HashTrigraph & g, const fast_map<vertex_t,int> & kleaves, vertex_t current){
    for(auto edge_type : {HashTrigraph::edge_t::black_edge, HashTrigraph::edge_t::red_edge}){
        for(auto v : g.get_neighbours(current, edge_type)){
            if (kleaves.count(v) == 0 || kleaves.at(v) >= kleaves.at(current)){
                return v;
            }
        }
    }
    return -1;
}

vertex_t get_any_child(const HashTrigraph & g, vertex_t current, vertex_t parent){
    for(auto edge_type : {HashTrigraph::edge_t::black_edge, HashTrigraph::edge_t::red_edge}){
        for(auto v : g.get_neighbours(current, edge_type)){
            if (v != parent){
                return v;
            }
        }
    }
    return -1;
}

fast_set<vertex_t> non_kleaf_neighbors(const HashTrigraph & g, const fast_map<vertex_t, int> & kleaves, vertex_t current){
    fast_set<vertex_t> neighborhood;
    for(auto edge_type : {HashTrigraph::edge_t::black_edge, HashTrigraph::edge_t::red_edge}){
        for(auto v : g.get_neighbours(current, edge_type)){
            if (!kleaves.contains(v)){
                neighborhood.insert(v);
            }
        }
    }
    return neighborhood;
}

void merge_and_add(HashTrigraph & g, vector<pair<vertex_t, vertex_t>>& merges, vertex_t u, vertex_t v){
    g.merge(u, v);
    merges.emplace_back(u, v);
    assert(g.get_degree(u, HashTrigraph::edge_t::red_edge) <= 2);
}


vector<int> reduce_same_level(HashTrigraph & g, fast_map<vertex_t,int> & kleaves, vertex_t current, vector<pair<vertex_t, vertex_t>>& merges, const fast_set<vertex_t> & forbidden){
    int k = kleaves.contains(current) ? kleaves[current] : 2;
    if(k == 0){
        return vector<int>();
    }
    vector<int> kaccumulator = vector<int>(k, -1);
    for(auto edge_type : {HashTrigraph::edge_t::red_edge, HashTrigraph::edge_t::black_edge}){
        auto neighbours = g.get_neighbours(current,edge_type);
        for(auto child : neighbours){
            if(forbidden.contains(child)){
                continue;
            }
            assert(kleaves.contains(child));
            int kchild = kleaves[child];
            assert(kchild < 2);
            if(kaccumulator[kchild] == -1){
                kaccumulator[kchild] = child;
            } else if (kchild == 0){
                merge_and_add(g, merges, kaccumulator[0], child);
            } else if(kchild == 1){
                merge_and_add(g, merges, kaccumulator[1], child);
                // the grandchildren can now be merged, we do that recursivelly
                reduce_same_level(g,kleaves,kaccumulator[1], merges, fast_set<vertex_t>({current}));
            } else{
                throw runtime_error("Vertex is kleaf of k > 2, this should have been reduced previously.");
            }
        }
    }
    if(kaccumulator.size() == 2 && kaccumulator[0] != -1 && kaccumulator[1] != -1){
        merge_and_add(g, merges, kaccumulator[1],kaccumulator[0]);
        kaccumulator[0] = -1;
    }
    for(int i = kaccumulator.size()-1; i >=0; --i){
        if(kaccumulator[i] == -1){
            kaccumulator.pop_back();
        } else{
            break;
        }
    }
    return kaccumulator;
}

void reduce(HashTrigraph & g, fast_map<vertex_t,int> & kleaves, vertex_t current, vector<pair<vertex_t, vertex_t>>& merges, vertex_t parent){
    assert(parent == -1 || g.has_vertex(parent));
    vector<int> kaccumulator = reduce_same_level(g, kleaves, current, merges, fast_set<vertex_t>({parent}));
    int k = kaccumulator.size();
    if(k == 0){
        return;
    }
    if(k == 2){
        if(kaccumulator[0] != -1){
            merge_and_add(g, merges, kaccumulator[1],kaccumulator[0]);
        }
        // 1leaf has to exist otherwise this node is not 2leaf
        vertex_t grandchild = get_any_child(g, kaccumulator[1], current);
        merge_and_add(g, merges, kaccumulator[1], grandchild);
        kleaves[current] = k = 1;
        kleaves[kaccumulator[1]] = 0;
        kaccumulator[0] = kaccumulator[1];
    }

    assert(k == 1 && "Should be reduced to 1leaf");
    if(parent == -1 || g.get_edge(current,parent) == HashTrigraph::edge_t::red_edge){
        merge_and_add(g,merges, current, kaccumulator[0]);
        kleaves[current] = 0;
    }
}

void tree_contraction_rule_long_paths(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges)
{
    // leaf is 0leaf
    // ileaf is a vertex which neighbours only those vertices that are jleafs for a j < i
    fast_map<vertex_t, int> kleaves;
    fast_map<vertex_t, vertex_t> parents;
    queue<vertex_t> q;
    fast_set<vertex_t> inq;
    for(auto v : g.get_vertices()){
        if(g.get_degree(v, HashTrigraph::edge_t::any) == 1){
            q.push(v);
            inq.insert(v);
        }
    }
    while(!q.empty()){
        vertex_t c = q.front();
        q.pop(); inq.erase(c);
        if(!g.has_vertex(c)){
            // vertex was already contracted
            continue;
        }
        int k = calc_kleaf(g,kleaves,c);
        if(k == -1){
            fast_set<vertex_t> neighborhood = non_kleaf_neighbors(g, kleaves, c);
            reduce_same_level(g,kleaves,c, merges, neighborhood);
            continue;
        }
        vertex_t parent = -1;
        if(parents.count(c) == 0){
            vertex_t parent = parents[c] = get_parent(g,kleaves,c);
            if(parent != -1 && !inq.contains(parent)){
                q.push(parent);
                inq.insert(parent);
            }
        }
        parent = parents[c];
        reduce(g, kleaves, c, merges, parent);
    }
}

bool tree_contraction_rule(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges){
    tree_contraction_rule_long_paths(g, merges);
    if(g.get_vertices_count() == 2 && g.get_edge_count(HashTrigraph::edge_t::any) == 1){
        auto u = *g.get_vertices().begin();
        auto v = *next(g.get_vertices().begin());
        merge_and_add(g, merges, u, v);
    }
    return false;
}

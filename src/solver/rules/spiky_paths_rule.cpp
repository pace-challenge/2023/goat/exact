#include "../rules.hpp"

#include "../../graph/hash_trigraph.hpp"
#include "../../fast_hash_table.hpp"
#include <vector>
#include <queue>
#include <stdexcept>
#include <cassert>

using namespace std;
using vertex_t = HashTrigraph::vertex_t;

struct hash_pair {
    template <class T1, class T2>
    size_t operator()(const pair<T1, T2>& p) const{
        auto hash1 = hash<T1>{}(p.first);
        auto hash2 = hash<T2>{}(p.second);
        if (hash1 != hash2) {
            return hash1 ^ hash2;             
        }
        return hash1;
    }
};


// TODO share with tree contraction rule
void merge_and_add2(HashTrigraph & g, vector<pair<vertex_t, vertex_t>>& merges, vertex_t u, vertex_t v){
    assert(g.has_vertex(u));
    assert(g.has_vertex(v));
    g.merge(u, v);
    merges.emplace_back(u, v);
    assert(g.get_degree(u, HashTrigraph::edge_t::red_edge) <= 2);
}

fast_map<vertex_t, vertex_t> get_relationships_map(const HashTrigraph & g){
    fast_map<vertex_t,vertex_t> sons;
    fast_set<vertex_t> visited;
    queue<vertex_t> q;
    for(auto v : g.get_vertices()){
        if(g.get_degree(v, HashTrigraph::edge_t::any) == 1){
            q.push(v);
            visited.insert(v);
        }
    }
    while(!q.empty()){
        vertex_t c = q.front();
        q.pop();
        if(g.get_degree(c, HashTrigraph::edge_t::any) <= 2){
            for(auto edge_type : {HashTrigraph::edge_t::black_edge, HashTrigraph::edge_t::red_edge}){
                for(auto v : g.get_neighbours(c, edge_type)){
                    if (!visited.contains(v)){
                        q.push(v);
                        visited.insert(v);
                        sons[v] = c;
                    }
                }
            }
        }
    }
    return sons;
}

vector<vertex_t> get_path(const HashTrigraph & g, const fast_map<vertex_t, vertex_t> & sons, fast_set<vertex_t> & visited, vertex_t v){
    vector<vertex_t> path;
    if(g.get_degree(v, HashTrigraph::edge_t::any) != 2 + sons.count(v)){
        return path;
    }
    path.push_back(v);
    visited.insert(v);
    bool changed = true;
    while(changed){
        changed = false;
        for(auto u : g.get_neighbours(v, HashTrigraph::edge_t::black_edge)){
            if(g.get_degree(u, HashTrigraph::edge_t::any) == 2 + sons.count(u) && !visited.contains(u)){ // not to get spikes
                v = u;
                path.push_back(v);
                visited.insert(v);
                changed = true;
                break;
            }
        }
    }
    return path;
}


void contract_spiky_path(HashTrigraph & g, vector<pair<vertex_t,vertex_t>> & merges, vector<vertex_t> & path, const fast_map<vertex_t, vertex_t> & sons){
    if(path.size() < 3){
        return;
    }
    // contracting inner spikes
    for(size_t i = 1; i < path.size() -1; ++i){
        vertex_t v = path[i];
        if(sons.contains(v)){
            vertex_t son = sons.at(v);
            if(sons.contains(son)){
                vertex_t grandson = sons.at(son);
                merge_and_add2(g, merges, son, grandson);
            }
            merge_and_add2(g, merges, v, son);
        }
    }
    // contract path
    for(size_t i = 1; i < path.size() -2; ++i){
        merge_and_add2(g, merges, path[i+1], path[i]);
    }
    vertex_t mid = path[path.size()-2];
    // contract outer spikes
    for(size_t i = 0; i < path.size(); i += path.size()-1){
        vertex_t v = path[i];
        if(sons.contains(v)){
            vertex_t son = sons.at(v);
            if(sons.contains(son)){
                vertex_t grandson = sons.at(son);
                merge_and_add2(g, merges, son, grandson);
            }
            merge_and_add2(g, merges, mid, son);
        }
    }
    // contracting the path
    path[1] = mid;
    path[2] = path[path.size()-1];
    path.resize(3);
}

vertex_t get_end(const HashTrigraph & g, vertex_t v, const fast_map<vertex_t, vertex_t> & sons){
    for(auto u : g.get_neighbours(v, HashTrigraph::edge_t::black_edge)){
        if(g.get_degree(u, HashTrigraph::edge_t::any) > 2 + sons.count(u)){ // not to get spikes
            return u;
        }
    }
    return -1;
}

void merge_paths(HashTrigraph & g, vector<pair<vertex_t, vertex_t>>& merges, const vector<vertex_t> & p1, const vector<vertex_t> & p2){
    merge_and_add2(g, merges, p1[0], p2[0]);
    merge_and_add2(g, merges, p1[2], p2[2]);
    merge_and_add2(g, merges, p1[1], p2[1]);
}

void merge_path_and_spikes(HashTrigraph & g, vector<pair<vertex_t, vertex_t>>& merges, const vector<vertex_t> & path, fast_map<vertex_t, vertex_t> & sons, vertex_t u){
    if(sons.contains(u)){
        vertex_t son = sons[u];
        merge_and_add2(g, merges, path[0], son);
        sons.erase(u);
        if(sons.contains(son)){
            merge_and_add2(g, merges, path[1], sons[son]);
        }
    }
}

void add_path(HashTrigraph & g, vector<pair<vertex_t, vertex_t>>& merges, fast_map_hash<pair<vertex_t,vertex_t>, vector<vertex_t>,hash_pair> & paths,
            vector<vertex_t> path, fast_map<vertex_t, vertex_t> & sons){
    assert(path.size() == 3);
    vertex_t u = get_end(g,path[0], sons);
    vertex_t v = get_end(g,path[path.size()-1], sons);
    if(u == -1 ||  v == -1){ // C3
        assert(g.get_edge(path[0], path[1]) != HashTrigraph::edge_t::no_edge);
        merge_and_add2(g,merges, path[0], path[1]);
        merge_and_add2(g,merges, path[0], path[2]);
        return;
    }
    if(u == v){ // cycle hanging via articulation
        merge_path_and_spikes(g, merges, path, sons, u);
        merge_and_add2(g,merges, path[0], path[2]);
        return;
    }
    if(u > v){
        swap(u,v);
        reverse(path.begin(), path.end());
    }
    pair<vertex_t, vertex_t> ends = make_pair(u, v);
    if(paths.contains(ends)){
        merge_paths(g, merges, paths[ends], path);
    } else{
        paths[ends] = path;
        merge_path_and_spikes(g, merges, path, sons, u);
        reverse(path.begin(), path.end());
        merge_path_and_spikes(g, merges, path, sons, v);
    }
}

bool spiky_path_rule(HashTrigraph &g, vector<pair<vertex_t, vertex_t>>& merges){
    size_t msize =  merges.size();
    fast_map_hash<pair<vertex_t,vertex_t>, vector<vertex_t>, hash_pair> paths;
    fast_map<vertex_t, vertex_t> sons = get_relationships_map(g);
    fast_set<vertex_t> visited;
    for(auto v : g.get_vertices()){
        if(g.get_degree(v, HashTrigraph::edge_t::black_edge) != 2 + sons.count(v) || visited.contains(v)){ // not to get spikes
            continue;
        }
        // now v is desirable
        visited.insert(v);
        vector<vertex_t> path, path2;
            for(auto u : g.get_neighbours(v, HashTrigraph::edge_t::black_edge)){
                if(g.get_degree(u, HashTrigraph::edge_t::black_edge) != 2 + sons.count(u) || visited.contains(u)){ // not to get spikes
                    continue;
                }
                if(path.size() == 0){
                    path = get_path(g, sons, visited, u);
                } else {
                    assert(path2.size() == 0);
                    path2 = get_path(g, sons, visited, u);
            }
        }
        reverse(path.begin(), path.end());
        path.push_back(v);
        path.insert(path.end(),path2.begin(), path2.end());
        if(path.size() <3){
            continue;
        }
        contract_spiky_path(g, merges, path, sons);
        add_path(g, merges, paths, path,sons);
    }
    return merges.size() != msize;
}

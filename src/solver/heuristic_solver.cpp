
#include <iostream>
#include <vector>
#include <utility>

#include "../timer/timer.h"
#include "options.hpp"
#include "../graph/hash_trigraph.hpp"
#include "../io/edge_list_graph_reader.hpp"
#include "../utils/utility.hpp"
#include "rules.hpp"
#include "../heuristics/bfs_tree_merging.hpp"

using namespace std;

using vertex_t = HashTrigraph::vertex_t;

using solve_component_func_t = std::function<bool(HashTrigraph&, vector<pair<vertex_t, vertex_t>>&)>;

//invariant: if this returns false, content of @merges remains unchanged!!
bool solve_component_bfs_merges(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges)
{
    if(g.get_vertices_count() == 1)
    {
        return true;
    }

    auto bfs_merges = bfs_tree_merging(g);
    for(const auto m : bfs_merges) {
        merges.push_back(m);
    }
    return true;
}

//invariant: if this returns false, content of @merges remains unchanged!!
bool solve_component_random(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges)
{
    if(g.get_vertices_count() == 1)
    {
        return true;
    }

    // random solution
    std::mt19937_64 gen64(42);
    auto vs = vector<vertex_t>(g.get_vertices().begin(), g.get_vertices().end());
    std::sort(vs.begin(), vs.end());
    std::shuffle(vs.begin(), vs.end(), gen64);
    for(size_t i = 0; i < vs.size() - 1; ++i) {
        merges.push_back({vs[i+1], vs[i]});
    }
    return true;
}

//invariant: if this returns false, content of @merges remains unchanged!!
bool solve_component_min_red_degree(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges)
{
    if(g.get_vertices_count() == 1)
    {
        return true;
    }

    std::cerr<<"comp="<<g.get_vertices_count()<<std::endl;

    size_t min_red_redgree_if_merge = g.get_vertices_count();
    vertex_t mu = -1, mv = -1;

    for(auto uit = g.get_vertices().begin(); uit != g.get_vertices().end(); ++uit)
    {
        for(auto vit = std::next(uit); vit != g.get_vertices().end(); ++vit)
        {
            const auto& u = *uit;
            const auto& v = *vit;
            size_t merge_red_deg = g.max_if_merge(u, v);
            if(merge_red_deg < min_red_redgree_if_merge) {
                min_red_redgree_if_merge = merge_red_deg;
                mu = u;
                mv = v;
            }
        }
    }

    assert(mu != -1 && mv != -1);
    HashTrigraph copy(g);
    copy.merge(mu, mv);
    merges.push_back({mu, mv});

    return solve_component_min_red_degree(copy, merges);
}

bool reduce_component_all(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges) {

    std::cerr<<"g.n="<<g.get_vertices_count()<<std::endl;

    while(1) {
        size_t nn = g.get_vertices_count();
        {
            TREE_TIMER(_rt, "twins_rule", "");
            twins_rule(g, merges);
        }
        std::cerr<<"g.n="<<g.get_vertices_count()<<std::endl;
        {
            TREE_TIMER(_rt, "tree_contraction_rule", "");
            tree_contraction_rule(g, merges);
        }
        std::cerr<<"g.n="<<g.get_vertices_count()<<std::endl;
        {
            TREE_TIMER(_rt, "spiky_path_rule", "");
            spiky_path_rule(g, merges);
        }
        std::cerr<<"g.n="<<g.get_vertices_count()<<std::endl;
        if(g.get_vertices_count() == nn) break;
    }
    return false;
}

std::pair<std::vector<std::pair<vertex_t, vertex_t>>, int> solve(const HashTrigraph& g, solve_component_func_t solve_component_func)
{
    vector<HashTrigraph> components = get_components(g);
    std::vector<vertex_t> singletons;
    std::vector<std::pair<vertex_t, vertex_t>> final_result;
    final_result.reserve(g.get_vertices_count() - 1);
    singletons.reserve(components.size());
    for(auto& component: components)
    {
        cerr<<"component g.n="<<component.get_vertices_count()<<endl;
        reduce_component_all(component, final_result);
        cerr<<"after reduce component g.n="<<component.get_vertices_count()<<endl;
        solve_component_func(component, final_result);
        if(component.get_vertices_count() == 1)
            singletons.push_back(*component.get_vertices().begin());
        else
            singletons.push_back(final_result.back().first);
    }

    for(size_t i = 1; i < singletons.size(); ++i)
        final_result.emplace_back(singletons[0], singletons[i]);

    return {final_result, get_highest_red_degree_of_merges(g, final_result)};
}

int main(int argc, char ** argv)
{
    Options options = read_options(argc, argv);

    try {

    while(1) {
        TREE_TIMER(_rt, "read", "");
        HashTrigraph G = EdgeListGraphReader<HashTrigraph>(
            1, [](HashTrigraph& g, vertex_t u, vertex_t v) { g.set_edge(u, v, HashTrigraph::edge_t::black_edge); }).read(std::cin);
        TREE_TIMER_STOP(_rt);

        std::cerr<<"g.n="<<G.get_vertices_count()<<", g.m="<<G.get_edge_count(HashTrigraph::edge_t::black_edge)<<std::endl;

        solve_component_func_t solve_component_func;
        if(options.heuristic_type == "bfs_merges") {
            solve_component_func = solve_component_bfs_merges;
        }
        else if(options.heuristic_type == "random") {
            solve_component_func = solve_component_random;
        }
        else if(options.heuristic_type == "min_red_degree") {
            solve_component_func = solve_component_min_red_degree;
        }
        else if(options.heuristic_type == "") {
            solve_component_func = solve_component_min_red_degree;
        }
        else {
            throw std::invalid_argument("Unknown heuristic_type=" + options.heuristic_type);
        }

        auto res = solve(G, solve_component_func);
        assert(res.second == (int)get_highest_red_degree_of_merges(G, res.first));
        assert(res.first.size() == G.get_vertices_count() - 1);
        if(!options.no_output_solution) {
        for(const auto& m: res.first)
        {
            cout << m.first + 1 << ' ' << m.second + 1 << endl;
        }
        }
        if(options.output_tww) {
            cout << "tww " << res.second << endl;
        }
        if(!options.read_multiple_instances) {
            break;
        }
    }

    } catch(const std::runtime_error& e) {}

    return 0;
}

#include <iostream>
#include <vector>
#include <utility>
#include <map>
#include <set>
#include <queue>

#include "options.hpp"
#include "../graph/hash_trigraph.hpp"
#include "../io/edge_list_graph_reader.hpp"
#include "../utils/utility.hpp"
#include "../heuristics/min_red_degree.hpp"
#include "rules.hpp"

using namespace std;

using vertex_t = HashTrigraph::vertex_t;

struct TwinWidthResult
{
	size_t twin_width;
	std::vector<std::pair<vertex_t, vertex_t>> merges;
};


bool reduce_all_exhaustively(HashTrigraph& g, std::vector<std::pair<HashTrigraph::vertex_t, HashTrigraph::vertex_t>>& merges, size_t max_red_deg) {
    while(1) {
        size_t nn = g.get_vertices_count();
        twins_rule(g, merges);
		if(max_red_deg>=2)
        tree_contraction_rule(g, merges);
        // spiky_path_rule(g, merges);
        sibling_rule(g, merges);
        if(g.get_vertices_count() == nn) break;
    }
    return false;
}

/**
 * @brief Given g, k = @max_red_deg, decides, whether G can be merged with error k. Invariant:
 * If this returns false, the contents of merges remains unchnged.
 */
bool solve_k(HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& merges, size_t seq_red_deg, size_t max_red_deg, size_t rec_depth)
{
	// std::cerr<<"solve_k start"<<std::endl;
	if(g.get_vertices_count() == 1)
	{
		return true;
	}

	// std::cerr<<"solve_k g.n="<<g.get_vertices_count()<<std::endl;

	reduce_all_exhaustively(g, merges, max_red_deg);
	// std::cerr<<"solve_k g.n="<<g.get_vertices_count()<<" after reduce_all_exhaustively"<<std::endl;

	if(g.get_vertices_count() == 1)
	{
		return true;
	}

	// auto get_min_red_degree_merge_r = get_min_red_degree_merge(g);
	// // std::cerr<<"solve_k lb="<<get_min_red_degree_merge_r.second<<std::endl;

	// if(get_min_red_degree_merge_r.second > max_red_deg) {
	// 	return false;
	// }

	// auto solve_min_red_degree_heuristic_r = solve_min_red_degree_heuristic(g);
	// // std::cerr<<"solve_k ub="<<solve_min_red_degree_heuristic_r.second<<std::endl;

	// if(solve_min_red_degree_heuristic_r.second <= seq_red_deg) {
	// 	merges.insert(merges.end(), solve_min_red_degree_heuristic_r.first.begin(), solve_min_red_degree_heuristic_r.first.end());
	// 	return true;
	// }

	// for(int i=0;i<rec_depth;++i)std::cerr<<" ";
	/*
	std::cerr<<"solve_k g.n="<<g.get_vertices_count()<< \
		", seq_red_deg="<<seq_red_deg<<", max_red_deg="<<max_red_deg<<", rec_depth="<<rec_depth<< \
		", lb="<<get_min_red_degree_merge_r.second<< \
		", ub="<<solve_min_red_degree_heuristic_r.second << \
		", rec_weft="<<rec_weft << \
		std::endl;
	*/

	set<pair<vertex_t, vertex_t>> small_dist_pairs;

	for(auto uit = g.get_vertices().begin(); uit != g.get_vertices().end(); ++uit) {
		const auto& s = *uit;

		queue<vertex_t> q;
		q.push(s);
		map<vertex_t, int> dist;
		dist[s] = 0;

		while(q.size()) {
			vertex_t u = q.front();
			q.pop();
			int du = dist[u];
			if(du >= 3) continue;

			for(vertex_t v : g.get_neighbours(u, HashTrigraph::edge_t::black_edge)) {
				if(dist.count(v)) continue;
				if(du + 1 >= 3) continue;
				dist[v] = du + 1;
				q.push(v);
			}
			for(vertex_t v : g.get_neighbours(u, HashTrigraph::edge_t::red_edge)) {
				if(dist.count(v)) continue;
				if(du + 1 >= 3) continue;
				dist[v] = du + 1;
				q.push(v);
			}
		}

		for(auto vd : dist) {
			vertex_t v = vd.first;
			if(s < v) small_dist_pairs.insert({s, v});
		}
	}

	// size_t rec_weft = 0;
	// size_t rec_weft_small = 0;
	// for(auto uit = g.get_vertices().begin(); uit != g.get_vertices().end(); ++uit) {
	// 	for(auto vit = std::next(uit); vit != g.get_vertices().end(); ++vit)
	// 	{
	// 		vertex_t u = *uit;
	// 		vertex_t v = *vit;
	// 		if( u > v) swap(u, v);


	// 		size_t max_if_merge_r = g.max_if_merge(u, v);
	// 		if(max_if_merge_r > max_red_deg)
	// 		{
	// 			continue;
	// 		}
	// 		rec_weft++;
	// 		if(small_dist_pairs.count({u, v})) {
	// 			rec_weft_small++;
	// 		}
	// 	}
	// }
	// cerr<<"---"<<endl;
	// cerr<<"rec_weft="<<rec_weft<<endl;
	// cerr<<"rec_weft_small="<<rec_weft_small<<endl;






	// for(auto uit = g.get_vertices().begin(); uit != g.get_vertices().end(); ++uit)
	// 	for(auto vit = std::next(uit); vit != g.get_vertices().end(); ++vit)
	for(auto p : small_dist_pairs)
		{
			vertex_t u = p.first;
			vertex_t v = p.second;
			// if( u > v) swap(u, v);

			// if (small_dist_pairs.count({u, v}) == 0) {
			// 	continue;
			// }

			size_t max_if_merge_r = g.max_if_merge(u, v);
			if(max_if_merge_r > max_red_deg)
			{
				continue;
			}

			//todo: tohle by se dalo zrychlit -- solve_component vrati, kolik vložila vrcholů a příslušný počet si pak odmažu z merges...
			HashTrigraph copy(g);
			copy.merge(u, v);
			std::vector<std::pair<vertex_t, vertex_t>> new_merges;
			new_merges.emplace_back(u, v);
			if(solve_k(copy, new_merges, max(max_if_merge_r, seq_red_deg), max_red_deg, rec_depth + 1))
			{
				merges.insert(merges.end(), new_merges.begin(), new_merges.end());
				return true;
			}
		}
	return false;
}

std::vector<HashTrigraph>& choose_components(std::vector<HashTrigraph>& g, std::vector<HashTrigraph>& g_compl)
{
	//TODO invent better choosing
	return g.size() > g_compl.size() ? g : g_compl;
}



void solve_rec(const HashTrigraph& g, vector<pair<vertex_t, vertex_t>>& out_merges, size_t& max_red_deg)
{
	if(g.get_vertices_count() == 1)
		return;

	while(1) {
		std::cerr<<"solve_rec max_red_deg="<<max_red_deg<<std::endl;
		std::vector<std::pair<vertex_t, vertex_t>> merges;
		HashTrigraph g_copy(g);
		if(solve_k(g_copy, merges, 0, max_red_deg, 0)) {
			out_merges.insert(out_merges.end(), merges.begin(), merges.end());
			return;
		}
		max_red_deg++;
	}

	// auto g_components = get_components(g);
	// for(auto & g_c : g_components) {
	// 	if(solve_k(g_c, merges, 0, max_red_deg, 0)) {
	// }

	// auto gcompl = g.complement();
	// auto gcomps = get_components(g);
	// auto gcomplcomps = get_components(gcompl);
	// auto& bigger = choose_components(gcomps, gcomplcomps);
	// if(bigger.size() == 1)
	// {
	// 	while(reduce_all_exhaustively(bigger[0], merges)) { }
	// 	while(!solve_k(bigger[0], merges, max_red_deg))
	// 		max_red_deg++;
	// }
	// else
	// {
	// 	std::vector<vertex_t> singletons;
	// 	for(auto& c: bigger)
	// 	{
	// 		solve_rec(c, merges, max_red_deg);
	// 		if(c.get_vertices_count() == 1)
	// 			singletons.push_back(*c.get_vertices().begin());
	// 		else
	// 			singletons.push_back(merges.back().first);
	// 	}
	// 	for(size_t i = 1; i < singletons.size(); ++i)
	// 		merges.emplace_back(singletons[0],singletons[i]);
	// }
}

TwinWidthResult solve(const HashTrigraph& g)
{
	TwinWidthResult result;
	result.twin_width = 0;
	solve_rec(g,result.merges,result.twin_width);
	return result;
}

int main(int argc, char** argv)
{
	Options options = read_options(argc, argv);

	try
	{

		while(1)
		{
			HashTrigraph G = EdgeListGraphReader<HashTrigraph>(
					1, [](HashTrigraph& g, vertex_t u, vertex_t v) { g.set_edge(u, v, HashTrigraph::edge_t::black_edge); }).read(std::cin);
			TwinWidthResult res = solve(G);
			// cerr<<"res="<<res.twin_width<<" "<<get_highest_red_degree_of_merges(G, res.merges)<<endl;
			// assert(res.twin_width == get_highest_red_degree_of_merges(G, res.merges));
			for(const auto& [u, v]: res.merges)
			{
				cout << u + 1 << ' ' << v + 1 << endl;
			}
			if(options.output_tww)
			{
				cout << "tww " << res.twin_width << endl;
			}
			if(!options.read_multiple_instances)
			{
				break;
			}
		}
	}
	catch(const std::runtime_error& e) { }

	return 0;
}
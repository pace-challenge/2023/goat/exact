-include local/config.mk

MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-builtin-variables

CXX          = g++
CXXFLAGS     = --std=c++17 -Ofast -Wall -Wextra -pedantic -g $(OPTIMIZE_FLAG) -D__DEBUG__ -DRTE_ON_FAILURE
DEPFLAGS     = -MT $@ -MMD -MP -MF ./build/$*.d

# checking if user has gurobi installed, if not then excluding ILP files
MAIN_FILES   = $(shell grep -lr 'int main' './src/' | grep 'cpp$$')
OTHER_FILES  = $(shell grep -Lr 'int main' './src/' | grep 'cpp$$')

SOURCE_FILES = $(OTHER_FILES) $(MAIN_FILES)
EXE_FILES    = $(MAIN_FILES:./src/%.cpp=./exe/%)
TEST_EXE_FLS = $(filter %test, $(EXE_FILES))
UNIT_TEST_EXE_FLS = $(filter %unit_test, $(EXE_FILES))
LONG_TEST_EXE_FLS = $(filter %long_test, $(EXE_FILES))
OBJECT_FILES = $(OTHER_FILES:./src/%.cpp=./build/%.o)
HEADER_FILES = $(OTHER_FILES:.cpp=.hpp)
DEPENDENCIES = $(SOURCE_FILES:./src/%.cpp=./build/%.d)

all: compile

compile: $(EXE_FILES)

./build/%.o: ./src/%.cpp ./build/%.d
	@mkdir -p `dirname $@`
	$(CXX) $(CXXFLAGS) $(DEPFLAGS) -c -o $@ $<

$(EXE_FILES): ./exe/%: ./src/%.cpp $(OBJECT_FILES)
	@mkdir -p `dirname $@`
	$(CXX) $(CXXFLAGS) -o $@ $< $(OBJECT_FILES) $(LOADLIBES)

test: $(TEST_EXE_FLS)
	@./test.sh

unit: $(UNIT_TEST_EXE_FLS)
	@./test.sh unit

long: $(LONG_TEST_EXE_FLS)
	@./test.sh long

.PHONY: clean
clean:
	@echo "don't change this rule, as it may be dangerous if small bugs are introduced"
	rm -rf build/ exe/

$(DEPENDENCIES):
include $(wildcard $(DEPENDENCIES))
